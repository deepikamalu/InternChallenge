#include<iostream>
#include<vector>
#include<algorithm>
int sumMissingNumbers(std::vector<int> arr) {
	int sum=0,j,c;
	sort(arr.begin(),arr.end());
	for(int i=0;i<arr.size()-1;i++){
			j=i;
			c=1;
			while(arr[j+1]!=arr[j]+c){
			    sum+=arr[j]+c;
			    c++;
			}
	}
	return sum;
}
int main(){
	int n,t;
	std::vector<int> vec;
	std::cout<<"Enter n:"<<std::endl;
	std::cin>>n;
	std::cout<<"Enter the values:"<<std::endl;
	for(int i=0;i<n;i++){
	    std::cin>>t;
	    vec.push_back(t);
    }
    std::cout<<sumMissingNumbers(vec)<<std::endl;
}
