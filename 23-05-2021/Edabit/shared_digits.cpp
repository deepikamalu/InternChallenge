#include<iostream>
#include<string>
#include<algorithm>
#include<vector>
#include<sstream>
using namespace std;
string find(int num){
	ostringstream str1;
	str1<<num;
	return str1.str();
}
bool sharedDigits(std::vector<int> arr) {
	string str1,str2;
	int c=0;
	for(int i=1;i<arr.size();i++){
		str1=find(arr.at(i-1));
		str2=find(arr.at(i));
		for(int j=0;j<str1.size();j++){
			if(count(str2.begin(),str2.end(),str1.at(j))!=0){
				c++;
				break;
			}
		}
	}
	if(c==arr.size()-1){
		return true;
	}
	else{
		return false;
	}
}
int main(){
	int n,t;
	std::vector<int> vec;
	std::cout<<"Enter n:"<<std::endl;
	std::cin>>n;
	std::cout<<"Enter the values:"<<std::endl;
	for(int i=0;i<n;i++){
	    std::cin>>t;
	    vec.push_back(t);
    }
    if(sharedDigits(vec)){
		std::cout<<"true"<<std::endl;
	}
	else{
		std::cout<<"false"<<std::endl;
	}
}
