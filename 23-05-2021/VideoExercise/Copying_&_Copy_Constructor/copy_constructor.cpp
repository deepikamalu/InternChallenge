#include<iostream>
//#include<string>
#include<cstring>
class String{
	private:
		char* m_buffer;
		int m_size;
	public:
	    String(const char* string){
	    	m_size=strlen(string);
	    	m_buffer=new char[m_size+1];
	    	memcpy(m_buffer,string,m_size);
	    	m_buffer[m_size]=0;
		}
		String(const String& other)
            :m_size(other.m_size){
            	m_buffer=new char[m_size+1];
            	memcpy(m_buffer,other.m_buffer,m_size+1);
			}
			char& operator[](int index){
	            return m_buffer[index];
            }
		friend std::ostream& operator<<(std::ostream& stream, String& other);
};
std::ostream& operator<<(std::ostream& stream, String& other){
	stream<<other.m_buffer<<" "<<other.m_size;
	return stream;
}
int main(){
	String str="Cherno";
	String str1=str;
	str1[2]='a';
	std::cout<<str<<std::endl;
	std::cout<<str1<<std::endl;
}
