#include<iostream>
#include<vector>
struct Vertex{
	int x,y,z;
	Vertex(int a,int b,int c)
	    :x(a),y(b),z(c){
		}
};
std::ostream& operator<<(std::ostream& stream, Vertex& other){   //like toString() in java
	stream<<other.x<<","<<other.y<<","<<other.z<<std::endl;
	return stream;
}
int main(){
//	Vertex e1(1,2,3);
//	Vertex e2(4,5,6);
//	Vertex e3(7,8,9);
	std::vector<Vertex> vec;
	vec.reserve(3);
	vec.emplace_back(1,2,3);
	vec.emplace_back(4,5,6);
	vec.emplace_back(7,8,9);
	for(int i=0;i<vec.size();i++){
		std::cout<<vec.at(i);
	}
	vec.erase(vec.begin()+1);
	for(int i=0;i<vec.size();i++){
		std::cout<<vec.at(i);
	}
	vec.clear();
}
