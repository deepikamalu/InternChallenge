#include<iostream>
#include<vector>
void func(std::vector<int> vec1){
	for(int i=0;i<vec1.size();i++){
		std::cout<<vec1.at(i);
	}
	std::cout<<std::endl;
}
int main(){
	std::vector<int> vec;
	for(int i=0;i<5;i++){
		vec.push_back(i);
	}
	func(vec);
	vec.erase(vec.begin()+1);
	func(vec);
	vec.clear();	
}
