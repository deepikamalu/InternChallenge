#include<iostream>
class Entity{
	public:
		Entity(){
			std::cout<<"Constructed Entity"<<std::endl;
		}
		~Entity(){
			std::cout<<"Destructed Entity"<<std::endl;
		}
};
int main(){
	Entity* e=new Entity();
	delete e;
}
