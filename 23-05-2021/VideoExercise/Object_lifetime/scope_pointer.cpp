#include<iostream>
class Entity{
	public:
		Entity(){
			std::cout<<"Constructed Entity"<<std::endl;
		}
		~Entity(){
			std::cout<<"Destructed Entity"<<std::endl;
		}
};
class Scopedptr{
	private:
		Entity* mptr;
	public:
		Scopedptr(Entity* m_ptr)
		    :mptr(m_ptr)
		{
			
		}
		~Scopedptr(){
			delete mptr;
		}
};
int main(){
	Scopedptr e=new Entity();
}
