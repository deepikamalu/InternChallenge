#include<iostream>
#include<string>
struct Vector{
	float x,y;
	Vector(float x,float y)
	    :x(x),y(y){
		}
	
	Vector add(const Vector& other) const{
	    return Vector(x+other.x,y+other.y);
	}	
	Vector operator+(const Vector& other) const{  //other-->ref speed  
		return add(other);
    }
    Vector multiply(const Vector& other) const{
	    return Vector(x*other.x,y*other.y);
	}	
	Vector operator*(const Vector& other) const{  //other--->ref powerup
		return multiply(other);
    }
};

std::ostream& operator<<(std::ostream& stream,const Vector& other){   //like toString() in java
	stream<<other.x<<","<<other.y;
	return stream;
}
int main(){
	Vector position(4.0f,4.0f);
	Vector speed(0.5f,1.5f);
	Vector powerup(1.1f,1.1f);
	Vector res1=position+speed*powerup;
	Vector res2=position.add(speed.multiply(powerup));
	std::cout<<res1<<std::endl;
}
