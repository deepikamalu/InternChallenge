#include<iostream>
#include<cstddef>
struct Offset{
	float x,y,z;
};
int main(){
	int offset1=(int)&((Offset*)nullptr)->x;
	int offset2=(int)&((Offset*)nullptr)->y;
	int offset3=(int)&((Offset*)nullptr)->z;
	std::cout<<offset1<<std::endl;
	std::cout<<offset2<<std::endl;
	std::cout<<offset3<<std::endl;
}
