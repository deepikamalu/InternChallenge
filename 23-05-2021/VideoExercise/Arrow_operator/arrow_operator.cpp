#include<iostream>
class Entity{
	public:
		Entity(){
			std::cout<<"Constructed Entity"<<std::endl;
		}
		~Entity(){
			std::cout<<"Destructed Entity"<<std::endl;
		}
		void print(){
			std::cout<<"Working of Arrow operator"<<std::endl;
		}
};
class Scopedptr{
	private:
		Entity* mptr;
	public:
		Scopedptr(Entity* m_ptr)
		    :mptr(m_ptr)
		{
			
		}
		~Scopedptr(){
			delete mptr;
		}
		Entity* operator->(){
			return mptr;
		}
		Entity* getObj(){
			return mptr;
		}
};
int main(){
	Scopedptr e=new Entity();
	e->print();
	e.getObj()->print();
}
