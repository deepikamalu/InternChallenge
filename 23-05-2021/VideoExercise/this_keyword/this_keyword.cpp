#include<iostream>
#include<string>
class Entity{
	public:
		int x,y;
		Entity(int x,int y){
			this->x=x;
			this->y=y;
		}
};
void print(const Entity& e){
	std::cout<<e.x<<" , "<<e.y<<std::endl;
}
int main(){
	Entity* entity=new Entity(10,20);
	print(*entity);
}
