#include<iostream>
#include<string>
#include<string.h>
class String{
    public:
    String()=default;
    String(const char* string){
        std::cout<<"Created\n"<<std::endl;
        m_size=strlen(string);
        m_data=new char[m_size];
        memcpy(m_data,string,m_size);
    }
    String(const String& other){
        std::cout<<"Copied\n"<<std::endl;
        m_size=other.m_size;
        m_data=new char[m_size];
        memcpy(m_data,other.m_data,m_size);
    }
    String(String&& other) noexcept{
        std::cout<<"Moved\n"<<std::endl;
        m_size=other.m_size;
        m_data=other.m_data;
        other.m_size=0;
        other.m_data=nullptr;
    }
    String& operator=(String&& other) noexcept{
	     std::cout<<"Moved\n"<<std::endl;
	     if(this!=&other){
	     delete[] m_data;
	     m_size=other.m_size;
	     m_data=other.m_data;
	     other.m_size=0;
	     other.m_data=nullptr;
	     return *this;
	 }
	}
    ~String(){
        std::cout<<"Destroyed\n"<<std::endl;
        delete m_data;
    }
    void print(){
        for(int i=0;i<m_size;i++){
            std::cout<<m_data[i]<<std::endl;
        }
        
    }
    private:
    char* m_data;
    int m_size;
};
class Entity{
    public:
    Entity(const String& name)
        :m_name(name){}
    Entity(String&& name)
        :m_name(std::move(name)){}
    void printname(){
        m_name.print();
    }
    private:
    String m_name;
};
int main(){
    Entity entity("Deepika");
    entity.printname();
    String str="Hello";
    String dest;
    dest=std::move(str);
}
    
