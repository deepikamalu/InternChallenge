#pragma once
#include "Header.h"
#include "ClassHeaders.h"
using namespace std;

class FileOperations {
public:
	void addAtSpecificLine(string prjName, string content, string name, int no);
	void addAtEnd(string prjName, string content, string name);
	void add(string prjName, string content, string config, int no, string unamel);
	void addNewContent();
	void removingline(string prjName, int n, string name);
	void removeContent(string prjName, int n, string config, string name);
	void removeAContent();
	void removingWholeContent(string prjName, string name);
	void removeWhole(string prjName, string config, string name);
	void removePrjContents();
	void writeContent(string prjName, string file,string name);
	void UpdateToAdd(string prjName, string content, string name, int no);
	void findToRemove(string prjName, int n, string name);
	void addingContentAndUpdateVersion(int max, string prjName, string content, string name);
	void addAtSpecificLineAndUpdateVersion(int max, string prjName, string content, string name, int no);
	void removinglineAndUpdateVersion(int max, string prjName, int n, string name);
	void removingWholeAndUpdateVersion(int max, string prjName, string name);
	

};