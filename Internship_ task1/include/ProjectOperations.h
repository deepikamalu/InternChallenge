#pragma once
#include "Header.h"
#include "ClassHeaders.h"
using namespace std;

class ProjectOperations {
public:
	void createProject();
	void updateProject();
	void updating(string prjName, string content, string ncont, string name);
	void update(string prjName, string content, string ncont, string conf, string name);
	void findToUpdateContent(string prjName, string content, string ncont, string name);
	void updatingContentAndUpdateVersion(int max, string prjName, string content, string newContent, string name);
	void revertProject();
	void displayProjectContents();
	void projectDisplays();
	void prjVersionDisplay();
	
};

