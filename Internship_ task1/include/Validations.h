#pragma once
#include "Header.h"
#include "ClassHeaders.h"


using namespace std;

class Validations {
public:
	string convertToString(int x);
	int convertToInt(string num);
	int linecount(string s, string str);
	string projectExists(string prj, string name, int n);
	string containsProject(string prj, string name, int n);
	string MailChecker(string mail);
	string PhoneChecker(string phn);
	string userExists(string val, int t, string str);
	string encrypt(string password);
	string validate(string password);
	void resetPassword(string mail);
	bool validDOB(int day, int month, int year);
	string validateProject(string pname, int n, string name);
	bool contains(string updateContent, string prjName, string name);
	string getUser();
	void detectConfigurations();
	string findConfiguration(string name, string prjName);
	string revertValidation(string prjName, string name, int n);
	string versionValidation(string prjName, string unamel, int n);
};


