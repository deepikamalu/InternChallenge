#include "Header.h"
#include "ClassHeaders.h"
using namespace std;

string verifyProject(string prjName, string name) {
	
	string  content, newprj, newprj1, res, unamel;
	Validations vp;
	newprj = vp.validateProject(prjName, 0, name);
	ifstream ifile;
	ifile.open("projectdata.txt");
	if (ifile) {
		
		newprj1 = vp.containsProject(newprj, name, 0);
	}
	else {
		newprj1 = newprj;
	}
	return newprj1;
}
void  ProjectOperations::createProject() {
	string prjName, content, newprj, newprj1, res, unamel;
	Validations vp;
	int cntt;
	std::cout << "Enter the project name:" << std::endl;
	std::cin >> prjName;
	std::cin.ignore();
	unamel = vp.getUser();
	newprj1 = verifyProject(prjName, unamel);
	std::cout << "Enter the content:" << std::endl;
	getline(cin, content);
	int l1 = vp.linecount("projectdata.txt","");
	
	int cnttt = 1000 + vp.linecount("projectdata.txt", "");
	
	string strrr = vp.convertToString(cnttt);
	res = strrr + " " + unamel + " " + newprj1 + " " + "10";
	string pname = newprj1.substr(0, (newprj1.length() - 2));
	string path = "C://Users//smile//source//repos//Zoho_Task1//" + unamel + "//" + pname;
	_mkdir(path.c_str());
	string pname1 = path + "\\" + newprj1;
	ofstream myfile2("projectdata.txt", ios::app);
	myfile2 << res << endl;
	myfile2.close();
	ofstream myfiled(pname1.c_str(), ios::app);
	myfiled << content << "\n";
	myfiled.close();
	time_t result = time(NULL);
	char sttrr[26];
	ctime_s(sttrr, sizeof sttrr, &result);
	string substr = prjName.substr(0, (prjName.length() - 2));
	string version = unamel + "/" + substr + "/" + substr + "_ projectVersion.txt";
	int l2 = vp.linecount(version.c_str(), unamel);
	string line1 = strrr + " " + newprj1 + " " + sttrr;
	ofstream myfilef(version.c_str(), ios::app);
	myfilef << line1 << endl;
	std::cout << "Successfully created the project" << std::endl;
}



//update
void ProjectOperations::updatingContentAndUpdateVersion(int max, string prjName, string content, string newContent, string name) {
	Validations vp;
	ProjectOperations po;
	FileOperations fo;
	string val = vp.convertToString(max);
	string file = prjName.substr(0, (prjName.length() - 2)) + val;
	string substr = prjName.substr(0, (prjName.length() - 2));
	string prjupdate = name + "/" + substr + "/" + file + "Changes";
	string projectPath = name + "/" + substr + "/" + prjName;
	string projectPath1 = name + "/" + substr + "/" + file;
	string line;
	ifstream ini_file(projectPath.c_str());
	ofstream out_file(projectPath1.c_str(), ios::app);

	if (ini_file.is_open()) {

		while (getline(ini_file, line)) {
			out_file << line << endl;

		}
	}
	ini_file.close();
	out_file.close();
	po.findToUpdateContent(file, content, newContent, name);

	string vall = "Updated a line in " + file;
	std::cout << vall << std::endl;
	ofstream myfilea(prjupdate.c_str(), ios::app);
	myfilea << vall << "\n";

	fo.writeContent(prjName, file,name);

}

void ProjectOperations::findToUpdateContent(string prjName, string content, string ncont, string name) {
	string substr = prjName.substr(0, (prjName.length() - 2));
	string projectPath = name + "/" + substr + "/" + prjName;
	const char* arr = "myfile.txt";
	ofstream myfiled("myfile.txt", ios::app);
	ifstream newfiled(projectPath.c_str(), ios::in);
	if (newfiled.is_open()) {
		string tp, sp;

		while (getline(newfiled, tp)) {
			if (tp.compare(content) == 0) {
				myfiled << ncont << "\n";
			}
			else {
				myfiled << tp << "\n";
			}
		}
	}
	myfiled.close();
	newfiled.close();
	ofstream filef(projectPath.c_str(), std::ofstream::out | std::ofstream::trunc);
	// ofstream filef(prj.c_str(), std::ios::trunc);
	ifstream ini_file1("myfile.txt", ios::in);
	ofstream out_file1(projectPath.c_str(), ios::app);
	string line;
	if (ini_file1.is_open()) {
		while (getline(ini_file1, line)) {
			out_file1 << line << "\n";
		}
	}
	ini_file1.close();
	out_file1.close();
	remove(arr);
}
//update a line in a file
void ProjectOperations::updating(string prjName, string content, string ncont, string name) {
	
	Validations vp;
	string substr = prjName.substr(0, (prjName.length() - 2));
	string prjUpdates = name + "/" + substr + "/" + prjName + "Changes";
	string projectPath = name + "/" + substr + "/" + prjName;
	findToUpdateContent(prjName, content, ncont, name);

	ofstream myfilefd(prjUpdates.c_str(), ios::app);
	string update = "Updated a line in " + prjName;
	std::cout << update << std::endl;
	myfilefd << update << endl;
}
//update a line in a file and creates new version


void findMethod2(string prjName, string content, string ncont, string conf, string name) {
	Validations vp;
	ProjectOperations po;
	int max = 0, l = 0;
	string num;
	int m = 0;
	string substr = prjName.substr(0, (prjName.length() - 2));
	string version = name + "/" + substr + "/" + substr + "_ projectVersion.txt";
	ifstream newfile(version.c_str(), ios::in);
	if (newfile) {
		num = prjName.substr(prjName.length() - 2);
		max = vp.convertToInt(num);
		std::vector<string> vec;
		if (newfile.is_open()) {
			string tp, sp;
			while (getline(newfile, tp)) {
				if (tp != "") {
					sp = "";
					for (int i = 0; i < tp.size(); i++) {

						if (tp.at(i) != ' ') {
							sp.append(1, tp.at(i));
						}
						else {
							vec.push_back(sp);
							sp = "";
						}
					}
					vec.push_back(sp);
					string ct = vec.at(1);
					int l1 = (ct.length() - 2);
					int l2 = (prjName.length() - 2);

					if (ct.substr(0, l1) == prjName.substr(0, l2)) {
						l = vp.convertToInt((vec.at(1)).substr((vec.at(1)).length() - 2));
						if (max < l) {
							max = l;
							m++;
						}
					}
				}
				vec.clear();
			}
		}
	}
	if (m > 0) {
		max = max + 1;
		po.updatingContentAndUpdateVersion(max, prjName, content, ncont, name);
	}
	else {
		num = prjName.substr(prjName.length() - 2);
		int maxt = vp.convertToInt(num);
		maxt = maxt + 1;
		po.updatingContentAndUpdateVersion(maxt, prjName, content, ncont, name);
	}
}
void ProjectOperations::update(string prjName, string content, string ncont, string conf, string name) {
	Validations vp;
	int conf1 = vp.convertToInt(conf);
	string substr = prjName.substr(0, (prjName.length() - 2));
	string prjupdates = name + "/" + substr + "/" + prjName + "Changes";
	int count = 0;
	ifstream ifile(prjupdates.c_str());
	if (!(ifile)) {
		updating(prjName, content, ncont, name);
	}
	else {
		count = vp.linecount(prjupdates, "");
		if (count % conf1 > 0) {
			updating(prjName, content, ncont, name);
		}
		else {
			findMethod2(prjName, content, ncont, conf, name);

		}
	}

}
void ProjectOperations::updateProject() {

	string prj, updateContent, newprj, validprj, conf, unamel, updateContent1;
	std::cout << "Enter the project name:" << std::endl;
	std::cin >> prj;
	std::cin.ignore();

	Validations vp;

	unamel = vp.getUser();
	validprj = vp.projectExists(prj, unamel, 1);
	int y = 1, c = 0;
	while (y) {
		std::cout << "These are the contents that are available in this project:" << std::endl;
		string substr = validprj.substr(0, (validprj.length() - 2));

		string projectPath = unamel + "/" + substr + "/" + validprj;
		ifstream new2file;
		new2file.open(projectPath.c_str(), ios::in);
		if (new2file.is_open()) {

			string line;
			while (getline(new2file, line)) {
				std::cout << c << ".  ";
				std::cout << line << std::endl;
				c++;
			}
			if (c == 0) {
				std::cout << "No data available" << std::endl;
			}
		}


		std::cout << "Enter the content to be updated:" << std::endl;
		getline(cin, updateContent);
		if (vp.contains(updateContent, validprj, unamel)) {
			y = 0;

		}
		else {
			std::cout << "Content not found" << std::endl;
		}
	}
	std::cout << "Enter the new content:" << std::endl;
	getline(cin, updateContent1);
	conf = vp.findConfiguration(unamel, validprj);
	update(validprj, updateContent, updateContent1, conf, unamel);

}


//revert

void fillContent(string validprj, string validprj1) {

	const char* arr1 = "myfile1.txt";
	ofstream myfill("myfile1.txt", ios::app);
	ifstream newfill("projectdata.txt", ios::in);
	std::vector<string> vec1;
	if (newfill.is_open()) {
		string tp, sp;
		while (getline(newfill, tp)) {
			sp = "";
			for (int i = 0; i < tp.size(); i++) {
				if (tp.at(i) != ' ') {
					sp.append(1, tp.at(i));
				}
				else {
					vec1.push_back(sp);
					sp = "";
				}
			}
			vec1.push_back(sp);
			if (vec1.at(2) == validprj) {
				string store = vec1.at(0) + " " + vec1.at(1) + " " + validprj1 + " " + vec1.at(3);
				myfill << store << "\n";
			}
			else {
				myfill << tp << "\n";
			}
			vec1.clear();
		}
	}
	myfill.close();
	newfill.close();
	ofstream fileef("projectdata.txt", std::ofstream::out | std::ofstream::trunc);
	fileef.close();
	ifstream ini_fileh1("myfile1.txt", ios::in);
	ofstream out_fileh1("projectdata.txt", ios::app);
	string lot;
	if (ini_fileh1.is_open()) {
		while (getline(ini_fileh1, lot)) {
			out_fileh1 << lot << endl;

		}
	}
	ini_fileh1.close();
	out_fileh1.close();
	remove(arr1);
	std::cout << validprj << " reverted to " << validprj1 << std::endl;
}

//revert

void ProjectOperations::revertProject() {
	string prj, validprj, unamel, prj1, validprj1;
	Validations vp;
	
	std::cout << "Enter the project to be reverted:" << std::endl;
	std::cin >> prj;
	std::cin.ignore();
	unamel =vp.getUser();
	validprj = vp.revertValidation(prj, unamel, 1);
	std::cout << "Enter the version in which the project to be reverted:" << std::endl;
	std::cin >> prj1;
	std::cin.ignore();
	validprj1 = vp.versionValidation(prj1, unamel, 1);
	fillContent(validprj, validprj1);

}

//display content
void ProjectOperations::displayProjectContents() {

	string prj, unamel;
	std::cout << "Enter the project:" << std::endl;
	std::cin >> prj;
	std::cin.ignore();
	
	Validations vp;
	
	unamel = vp.getUser();

	int c = 0;
	string validprj = vp.projectExists(prj, unamel, 1);
	string substr = validprj.substr(0, (validprj.length() - 2));

	string projectPath = unamel + "/" + substr + "/" + validprj;
	ifstream new2file;
	new2file.open(projectPath.c_str(), ios::in);
	if (new2file.is_open()) {

		string line;
		while (getline(new2file, line)) {
			std::cout << c << ".  ";
			std::cout << line << std::endl;
			c++;
		}
		if (c == 0) {
			std::cout << "No data available" << std::endl;
		}
	}

}

//display projects

void ProjectOperations::projectDisplays() {
	string unamel;
	
	Validations vp;
	
	unamel = vp.getUser();
	ifstream newfill;
	newfill.open("projectdata.txt", ios::in);
	std::vector<string> vec1;
	if (newfill.is_open()) {
		string line, sp;
		while (getline(newfill, line)) {
			sp = "";
			for (int i = 0; i < line.size(); i++) {
				if (line.at(i) != ' ') {
					sp.append(1, line.at(i));
				}
				else {
					vec1.push_back(sp);
					sp = "";
				}
			}
			vec1.push_back(sp);
			if (vec1.at(1) == unamel) {
				std::cout << vec1.at(2) << "\n";
			}
			vec1.clear();
		}
	}
}

//display versions

void filterVersions(string validprj,string unamel) {
	string substr = validprj.substr(0, (validprj.length() - 2));
	string version = unamel + "/" + substr + "/" + substr + "_ projectVersion.txt";
	ifstream newfile(version.c_str());
	if (newfile.is_open()) {
		string line, sp;
		std::vector<string> vec1;
		while (getline(newfile, line)) {
			// stringstream ss(tp);
			if (line != "") {
				sp = "";
				for (int i = 0; i < line.size(); i++) {
					if (line.at(i) != ' ') {
						sp.append(1, line.at(i));
					}
					else {
						vec1.push_back(sp);
						sp = "";
					}
				}
				vec1.push_back(sp);
				string str = vec1.at(1);
				if ((validprj.substr(0, (validprj.length() - 2))) == (str.substr(0, (str.length() - 2)))) {
					for (int i = 1; i < vec1.size(); i++) {
						std::cout << vec1.at(i) << " ";
					}
					std::cout << "\n";
				}
			}
			vec1.clear();
		}
	}
}



void ProjectOperations::prjVersionDisplay() {

	string prj, validprj, unamel, prj1;
	std::cout << "Enter the project name:" << std::endl;
	std::cin >> prj;
	std::cin.ignore();
	
	Validations vp;
	
	unamel = vp.getUser();
	validprj = vp.revertValidation(prj, unamel, 1);
	filterVersions(validprj,unamel);

}

