#include "Header.h"
#include"ClassHeaders.h"

using namespace std;

void StartOperations::ini_Operations(string userName) {
	Validations vp;
	ProjectOperations po;
	FileOperations fo;
	

	
	while (1) {
		std::cout << "a. Create project:" << std::endl;
		std::cout << "b. Add data:" << std::endl;
		std::cout << "c. Updata data:" << std::endl;
		std::cout << "d. Remove a line:" << std::endl;
		std::cout << "e. Remove whole content:" << std::endl;
		std::cout << "f. Display data:" << std::endl;
		std::cout << "g. Revert:" << std::endl;
		std::cout << "h. Configure:" << std::endl;
		std::cout << "i. Version display" << std::endl;
		std::cout << "j. Display my projects" << std::endl;
		std::cout << "k. exit(0)" << std::endl;
		char ch;
		std::cout << "Enter your choice:" << std::endl;
		std::cin >> ch;
		switch (ch) {
		case 'a':
		{
			po.createProject();
			break;
		}
		case 'b':
		{
			fo.addNewContent();
			break;
		}
		case 'c':
		{
			po.updateProject();
			break;
		}
		case 'd':
		{
			fo.removeAContent();
			break;
		}
		case 'e':
		{
			fo.removePrjContents();
			break;
		}
		case 'f':
		{
			po.displayProjectContents();
			break;
		}
		case 'g':
		{
			po.revertProject();
			break;
		}
		case 'h':
		{
			vp.detectConfigurations();
			break;
		}
		case 'i':
		{
			po.prjVersionDisplay();
			break;
		}
		case 'j':
		{
			po.projectDisplays();
			break;
		}
		case 'k':
		{
			exit(0);
		}
		default:
		{
			std::cout << "Invalid option\n";
		}
		}
	}
}