#include "Header.h"
#include "ClassHeaders.h"

#define __STDC_WANT_LIB_EXT1__ 1
using namespace std;

string Validations::convertToString(int x) {
	string strr;
	stringstream ss;
	ss << x;
	ss >> strr;
	return strr;
}

//convert string to int
int Validations::convertToInt(string num) {
	stringstream find(num);
	int x = 0;
	find >> x;
	return x;
}

//counting no of lines in a file
int Validations::linecount(string str,string uname) {
	
	string ptr = str.substr(0, (str.length() - 2));
	string path;
	if (!(uname.empty())) {
		path = "C:/Users/smile/source/repos/Zoho_Task1/"+uname + "/" + ptr + "/" + str;
		
	}
	else {
		path = str;
	}
	int Total = 0;
	string line;
	ifstream newfile;
	newfile.open(path.c_str(),ios::in);

	if (newfile.is_open()) {
		
		while (getline(newfile, line)) {
			
			Total++;
			
		}
	}
	
	return Total;
}

void alterCount(string operations) {
	const char* arr1 = "myfile1.txt";
	ofstream myfill("myfile1.txt", ios::app);
	ifstream newfill("projectdata.txt", ios::in);
	std::vector<string> vec1;
	if (newfill.is_open()) {
		string line, sp;
		while (getline(newfill, line)) {
			sp = "";
			for (int i = 0; i < line.size(); i++) {
				if (line.at(i) != ' ') {
					sp.append(1, line.at(i));
				}
				else {
					vec1.push_back(sp);
					sp = "";
				}
			}
			vec1.push_back(sp);
			string store = vec1.at(0) + " " + vec1.at(1) + " " + vec1.at(2) + " " + operations;
			myfill << store << "\n";
			vec1.clear();
		}
	}
	myfill.close();
	newfill.close();
	ofstream fileef("projectdata.txt", std::ofstream::out | std::ofstream::trunc);
	fileef.close();
	ifstream ini_fileh1("myfile1.txt", ios::in);
	ofstream out_fileh1("projectdata.txt", ios::app);
	string lot;
	if (ini_fileh1.is_open()) {
		while (getline(ini_fileh1, lot)) {
			out_fileh1 << lot << endl;
		}
	}
	ini_fileh1.close();
	out_fileh1.close();
	remove(arr1);
}
void Validations::detectConfigurations() {
	string operations;
	std::cout << "Enter the number of operations to be performed in a file:" << std::endl;
	std::cin >> operations;
	alterCount(operations);
}

string Validations::validateProject(string prjName, int n,string name) {
	string end = "11", str1;
	int c1 = 0, c2 = 0, t = 0;
	if (prjName.length() > end.length()) {
		c2 = 1;
		if (prjName.compare(prjName.length() - end.length(), end.length(), end) == 0) {
			c1 = 1;
		}
		else if (n == 1) {
			string f1 = prjName.substr(0, (prjName.length() - 2));
			string path = name + "/" + f1 + "/" + f1 + end;
			ifstream f2(path.c_str());
			if (f2) {
				c1 = 1;
			}
			else {

				std::cout << "Should end with 11" << std::endl;
			}
		}
		else {
			std::cout << "Should end with 11" << std::endl;
		}
	}
	else {
		std::cout << "Should contain atleast 3 characters" << std::endl;
	}
	if (c1 == 0 || c2 == 0 || t == 1) {
		std::cout << "Enter the project name:" << std::endl;
		std::cin >> prjName;
		std::cin.ignore();
		str1 = validateProject(prjName, n,name);
	}
	else {
		str1 = prjName;
	}
	return str1;
}
string Validations::projectExists(string prjName, string name, int n) {
	ifstream newfile("projectdata.txt", ios::in);
	std::vector<string> vec;
	string reval, prj1, newprj, prj2, prj3;
	int ct = 0, nt = 0;
	if (newfile.is_open()) {
		string tp, sp;

		while (getline(newfile, tp)) {
			// stringstream ss(tp);
			if (tp != "") {
				sp = "";

				for (int i = 0; i < tp.size(); i++) {

					if (tp.at(i) != ' ') {
						sp.append(1, tp.at(i));
					}
					else {
						vec.push_back(sp);
						sp = "";
					}
				}
				vec.push_back(sp);

				if (vec.at(1) == name) {

					if (vec.at(2) == prjName) {
						ct = 1;
						break;
					}
				}
			}
			vec.clear();
		}
	}
	if (ct == 0) {
		std::cout << "Project not found" << std::endl;
		std::cout << "Enter project name :" << std::endl;
		std::cin >> prj2;
		std::cin.ignore();
		prj3 = validateProject(prj2, n,name);
		reval = projectExists(prj3, name, n);

	}
	else {
		reval = prjName;
	}
	return reval;
}


string Validations::containsProject(string prjName, string name, int n) {
	ifstream newfile;
	
	newfile.open("projectdata.txt");
	std::vector<string> vec;
	string reval, prj1, newprj, prj2, prji;
	int ct = 0, nt = 0;
	if (newfile.is_open()) {
		string tp, sp;
		
		while (getline(newfile, tp)) {
			// stringstream ss(tp);
			sp = "";

			for (int i = 0; i < tp.size(); i++) {

				if (tp.at(i) != ' ') {
					sp.append(1, tp.at(i));
				}
				else {
					vec.push_back(sp);
					sp = "";
				}
			}
			vec.push_back(sp);
			
			if (vec.at(1) == name) {
				if (vec.at(2) == prjName) {
					
					std::cout << "Project already found" << std::endl;
					nt = 1;
					break;
				}
			}
			vec.clear();
		}
	}
	
	if (nt == 1) {
		std::cout << "Enter project name :" << std::endl;
		std::cin >> prj2;
		std::cin.ignore();
		prji = validateProject(prj2, n,name);
		reval = containsProject(prji, name, n);
	}
	else {
		reval = prjName;
	}
	return reval;
}

string Validations::MailChecker(string mail) {
	string reval;
	if (mail.find("@gmail.com") == -1) {
		std::cout << "Email should ends with @gmail.com" << std::endl;
		string val;
		std::cout << "Reenter Email :" << std::endl;
		std::cin >> val;
		std::cin.ignore();
		reval = MailChecker(val);
	}
	else {
		reval = mail;
	}
	return reval;
}
string Validations::PhoneChecker(string phn) {
	int c = 0;
	string reval;
	if (phn.length() == 10) {
		for (int i = 0; i < phn.length(); i++) {
			if (!(phn.at(i) - 48 >= 0 && phn.at(i) - 48 <= 9)) {
				c = 1;
				break;
			}
		}
		if (c == 1) {
			std::cout << "Should contain only numbers" << std::endl;
			//string val;
			std::cout << "reenter phone number :" << std::endl;
			std::cin >> phn;
			std::cin.ignore();
			reval = PhoneChecker(phn);
		}
		else {
			reval = phn;
		}
	}
	else {
		std::cout << "Should be of length 10" << std::endl;
		//string val;
		std::cout << "Reenter phone number :" << std::endl;
		std::cin >> phn;
		std::cin.ignore();
		reval = PhoneChecker(phn);
	}
	return reval;
}
string Validations::userExists(string data, int t, string str) {
	ifstream newfile("userdata.txt");
	string reval;
	int c = 0;

	if (newfile.is_open()) {
		string line, sp;
		while (getline(newfile, line)) {
			stringstream ss(line);
			while (getline(ss, sp, ' ')) {

				if (sp == data) {

					c++;
					break;
				}
				sp = "";
			}
		}
		if (c > 0) {
			std::cout << str << " already exists" << std::endl;
			int v = 0;
			string val;
			std::cout << "reenter " << str << " :" << std::endl;
			std::cin >> val;
			std::cin.ignore();
			reval = userExists(val, t, str);
		}
		else {
			reval = data;
		}
	}
	return reval;
}
string Validations::findConfiguration(string name, string prjName) {

	ifstream newfile("projectdata.txt");
	std::vector<string> vec;
	string reval, prj1, newprj, prj2, prji, conf;
	int ct = 0, nt = 0;
	if (newfile.is_open()) {
		string tp, sp;
		while (getline(newfile, tp)) {
			// stringstream ss(tp);
			sp = "";
			for (int i = 0; i < tp.size(); i++) {
				if (tp.at(i) != ' ') {
					sp.append(1, tp.at(i));
				}
				else {
					vec.push_back(sp);
					sp = "";
				}
			}
			vec.push_back(sp);
			if (vec.at(1) == name) {
				if (vec.at(2) == prjName) {
					conf = vec.at(3);
					break;
				}
			}
			vec.clear();
		}
	}

	return conf;
}

string Validations::revertValidation(string prjName, string name, int n) {
	Validations vp;
	ifstream newfile("projectdata.txt");
	std::vector<string> vec;
	string reval, prj1, newprj, prj2, prji;
	int ct = 0, nt = 0, c1 = 2, c2 = 2;
	if (newfile.is_open()) {
		string tp, sp;
		while (getline(newfile, tp)) {
			sp = "";
			for (int i = 0; i < tp.size(); i++) {
				if (tp.at(i) != ' ') {
					sp.append(1, tp.at(i));
				}
				else {
					vec.push_back(sp);
					sp = "";
				}
			}
			vec.push_back(sp);
			string str = vec.at(2);
			if (vec.at(1) == name) {
				if (((prjName.substr(0, (prjName.length() - 2))) == (str.substr(0, (str.length() - 2)))) && (prjName != str)) {
					c1 = 1;
					c2 = 0;
				}
				else if (prjName != str) {
					c1 = 0;
					c2 = 1;
				}
				else {
					c1 = 0;
					c2 = 0;
					break;
				}
			}
			vec.clear();
		}
		if (c1 == 1) {
			std::cout << "Current version should be given" << std::endl;
		}
		if (c2 == 1 || c2 == 2 || c1 == 2) {
			std::cout << "Project not found" << std::endl;
		}
		if (c1 == 1 || c2 == 1 || c2 == 2 || c1 == 2) {
			std::cout << "Enter project name :" << std::endl;
			std::cin >> prj2;
			std::cin.ignore();
			prji = vp.validateProject(prj2, n, name);
			reval = revertValidation(prji, name, n);
		}
		else {
			reval = prjName;
		}
	}
	return reval;
}

string Validations::versionValidation(string prjName, string unamel, int n) {
	Validations vp;
	string substr = prjName.substr(0, (prjName.length() - 2));
	string version = unamel + "/" + substr + "/" + substr + "_ projectVersion.txt";
	
	ifstream newfile(version.c_str());
	string reval, newprj, prj2, prji;
	int ct = 0, nt = 0, c1 = 0, c2 = 0;
	if (newfile.is_open()) {
		string tp, sp;
		std::vector<string> vec;
		while (getline(newfile, tp)) {
			if (tp != "") {
				sp = "";
				for (int i = 0; i < tp.size(); i++) {
					if (tp.at(i) != ' ') {
						sp.append(1, tp.at(i));
					}
					else {
						vec.push_back(sp);
						sp = "";
					}
				}
				vec.push_back(sp);
				string str = vec.at(1);
				if ((prjName.substr(0, (prjName.length() - 2))) == (str.substr(0, (str.length() - 2)))) {
					if (str.compare(prjName) == 0) {
						c1 = 1;
						break;
					}
					else {
						c1 = 2;
					}
				}
			}
			vec.clear();
		}
	}
	if (c1 == 0) {
		std::cout << "Project not found" << std::endl;
	}
	else if (c1 == 2) {
		std::cout << "Project version not found" << std::endl;
	}
	if (c1 == 0 || c1 == 2) {
		std::cout << "Enter project name :" << std::endl;
		std::cin >> prj2;
		std::cin.ignore();
		prji = vp.validateProject(prj2, n, unamel);
		reval = versionValidation(prji, unamel, n);

	}
	else {
		reval = prjName;
	}

	return reval;
}


string Validations::encrypt(string password) {
	static const int n = password.length();
	char arr[10];
	string encryptPassword;
	int k = 0;
	for (int i = 0; i < password.length(); i++) {
		arr[k++] = password.at(i);
	}
	arr[k++] = '\0';


	for (int i = 0; (i < 100 && arr[i] != '\0'); i++) {
		arr[i] = arr[i] + 2;
		encryptPassword.append(1, arr[i]);
	}
	return encryptPassword;
}
string Validations::validate(string password) {
	string reEnterPassword;
	bool y;
	y = false;
	static const int n = password.length() + 1;
	char a[10];
	int res = 0;
	int k = 0;
	for (int i = 0; i < password.length(); i++) {
		a[k++] = password.at(i);
	}
	a[k++] = '\0';

	a[password.length()] = '\0';
	int l = strlen(a);
	if (l >= 5 && l <= 10) {
		int c = 0, n = 0;
		for (int i = 0; i < l; i++) {
			if (a[i] >= '0' && a[i] <= '9') {
				n++;
				continue;
			}
			y = ((a[i] >= 'a' && a[i] <= 'z') || (a[i] >= 'A' && a[i] <= 'Z'));
			if (y == false) {
				c++;
			}
			if (a[i] == ' ') {
				c = 0;
				n = 0;
				break;
			}
		}
		if (n > 0 && c > 0)
			res = 1;
		else {
			if (n == 0) {
				cout << "Password doesn't have any digits!";
				res = 0;
			}
			else {
				cout << "Password doesn't have any special character!";
				res = 0;
			}
		}
	}
	else {
		cout << "Password too short or long!";
		res = 0;
	}
	if (res == 0) {
		string password1;
		std::cout << "Reenter password :" << std::endl;
		std::cin >> password1;
		reEnterPassword = validate(password1);
	}
	else {
		reEnterPassword = password;
	}
	return reEnterPassword;

}
string Validations::getUser() {
	string prjName, content, newprj, newprj1, res, unamel;
	Validations vp;
	vector<string> vec;
	ifstream new1file("logindata.txt", ios::in);
	// string reval,prj1;
	int ct = 0, kt = 0;
	if (new1file.is_open()) {
		string tp, sp = "";
		while (getline(new1file, tp)) {

			kt++;
			if (kt == vp.linecount("logindata.txt", "")) {

				for (int i = 0; i < tp.size(); i++) {
					if (tp.at(i) != ' ') {
						sp.append(1, tp.at(i));
					}
					else {
						vec.push_back(sp);
						sp = "";
					}
				}
				vec.push_back(sp);
				unamel = vec.at(1);
			}
			vec.clear();
		}
	}
	return unamel;
}

bool Validations::contains(string updateContent, string prjName, string name) {
	int cnt = 0;
	string line;
	
	string substr = prjName.substr(0, (prjName.length() - 2));
	string projectPath = name + "/" + substr + "/" + prjName;
	ifstream ini_file;
	ini_file.open(projectPath.c_str(), ios::in);

	if (ini_file.is_open()) {
		
		while (getline(ini_file, line)) {
			if (updateContent.compare(line) == 0) {
				cnt = 1;
				break;
			}
		}
	}
	ini_file.close();
	if (cnt == 1) {
		return true;
	}
	else {
		return false;
	}


}
void Validations::resetPassword(string mail) {
	string pwd, reEnterPassword, password1, place;
	int c = 0, t = 0;
	while (t == 0) {
		std::cout << "Enter your favorite place:" << std::endl;
		std::cin >> place;
		ifstream myfile("userdata.txt", ios::in);
		std::vector<string> vec1;
		if (myfile.is_open()) {
			string sp, tp;
			while (getline(myfile, tp)) {
				sp = "";
				for (int i = 0; i < tp.size(); i++) {

					if (tp.at(i) != ' ') {
						sp.append(1, tp.at(i));
					}
					else {
						vec1.push_back(sp);
						sp = "";
					}
				}

				vec1.push_back(sp);

				if (vec1.at(3) == mail) {

					if (vec1.at(7) == place) {

						c = 1;
					}
				}

				vec1.clear();
			}
		}
		myfile.close();
		if (c == 1) {

			t = 1;
			std::cout << "Enter the password for resetting:" << std::endl;
			std::cin >> pwd;
			reEnterPassword = validate(pwd);

			password1 = encrypt(reEnterPassword);

			const char* arr1 = "myfile.txt";
			ofstream mfile("myfile.txt", ios::app);
			ifstream myfiled("userdata.txt", ios::in);
			std::vector<string> vec2;
			if (myfiled.is_open()) {
				string spp, tpp;

				while (getline(myfiled, tpp)) {
					if (tpp != "") {
						spp = "";
						for (int i = 0; i < tpp.size(); i++) {

							if (tpp.at(i) != ' ') {
								spp.append(1, tpp.at(i));
							}
							else {
								vec2.push_back(spp);
								spp = "";
							}
						}
						vec2.push_back(spp);

						if (vec2.at(3) == mail) {
							if (vec2.at(7) == place) {
								string store = vec2.at(0) + " " + vec2.at(1) + " " + vec2.at(2) + " " + vec2.at(3) + " " + vec2.at(4) + " " + vec2.at(5) + " " + password1 + " " + vec2.at(7);

								mfile << store << "\n";
							}
						}
						else {

							mfile << tpp << "\n";
						}

						vec2.clear();
					}

				}
			}

			myfiled.close();
			mfile.close();
			ofstream fileef("userdata.txt", std::ofstream::out | std::ofstream::trunc);
			fileef.close();
			ifstream ini_fileh1("myfile.txt", ios::in);
			ofstream out_fileh1("userdata.txt", ios::app);
			string line;
			if (ini_fileh1.is_open()) {
				while (getline(ini_fileh1, line)) {

					if (line != "") {

						out_fileh1 << line << endl;
					}
				}
			}

			ini_fileh1.close();
			out_fileh1.close();
			remove(arr1);
		}
		else {
			std::cout << "Doesn't match" << std::endl;
		}
	}
}

bool Validations::validDOB(int day, int month, int year) {
	if (1000 <= year && year <= 3000)
	{
		if ((month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) && day > 0 && day <= 31)
			return true;
		else

			if (month == 4 || month == 6 || month == 9 || month == 11 && day > 0 && day <= 30)
				return true;
			else
				if (month == 2)
				{
					if ((year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) && day > 0 && day <= 29)
						return true;
					else if (day > 0 && day <= 28)
						return true;
					else
						return false;
				}
				else
					return false;
	}
	else
		return false;
}

