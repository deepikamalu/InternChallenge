#include "Header.h"
#include "ClassHeaders.h"

using namespace std;

void FileOperations::writeContent(string prjName, string file,string name) {
	Validations vp;
	
	const char* arr = "myfile.txt";
	ofstream myfiled("myfile.txt", ios::app);
	ifstream newfiled("projectdata.txt", ios::in);
	std::vector<string> vec;
	if (newfiled.is_open()) {
		string tp, sp;
		while (getline(newfiled, tp)) {
			sp = "";
			for (int i = 0; i < tp.size(); i++) {
				if (tp.at(i) != ' ') {
					sp.append(1, tp.at(i));
				}
				else {
					vec.push_back(sp);
					sp = "";
				}
			}
			vec.push_back(sp);
			if (vec.at(2) == prjName) {
				string h = vec.at(0) + " " + vec.at(1) + " " + file + " " + vec.at(3);
				myfiled << h << "\n";
			}
			else {
				myfiled << tp << "\n";
			}
			vec.clear();
		}
	}
	myfiled.close();
	newfiled.close();
	ofstream filef("projectdata.txt", std::ofstream::out | std::ofstream::trunc);
	filef.close();
	ifstream ini_file1("myfile.txt", ios::in);
	ofstream out_file1("projectdata.txt", ios::app);
	string lo;
	if (ini_file1.is_open()) {
		while (getline(ini_file1, lo)) {
			out_file1 << lo << endl;
		}
	}
	ini_file1.close();
	out_file1.close();
	remove(arr);
	string substr = prjName.substr(0, (prjName.length() - 2));
	string version = name + "/" + substr + "/" + substr + "_ projectVersion.txt";
	ofstream myfilet(version.c_str(), ios::app);
	ifstream newfilet("projectdata.txt", ios::in);
	std::vector<string> vec1;
	int ll2 = vp.linecount(version.c_str(), "");
	if (newfilet.is_open()) {
		string tp, sp;
		while (getline(newfilet, tp)) {
			sp = "";
			for (int i = 0; i < tp.size(); i++) {
				if (tp.at(i) != ' ') {
					sp.append(1, tp.at(i));
				}
				else {
					vec1.push_back(sp);
					sp = "";
				}
			}
			vec1.push_back(sp);
			time_t result = time(NULL);
			char sttrr[26];
			ctime_s(sttrr, sizeof sttrr, &result);
			if (vec1.at(2) == file) {
				myfilet << vec1.at(0) + " " + file + " " + sttrr << endl;
				break;
			}
			vec1.clear();
		}
	}
}

//add
void FileOperations::UpdateToAdd(string prjName, string content, string name, int no) {
	const char* arr = "myfile.txt";
	string substr = prjName.substr(0, (prjName.length() - 2));
	string projectPath = name + "/" + substr + "/" + prjName;
	int c = 0;
	ofstream myfiled("myfile.txt", ios::app);
	ifstream newfiled(projectPath.c_str(), ios::in);
	if (newfiled.is_open()) {
		string tp, sp;

		while (getline(newfiled, tp)) {

			if (c == no) {
				myfiled << content << "\n";
				myfiled << tp << "\n";
			}
			else {
				myfiled << tp << "\n";
			}
			c++;
		}
	}
	myfiled.close();
	newfiled.close();
	ofstream filef(projectPath.c_str(), std::ofstream::out | std::ofstream::trunc);
	ifstream ini_file1("myfile.txt", ios::in);
	ofstream out_file1(projectPath.c_str(), ios::app);
	string line;
	if (ini_file1.is_open()) {

		while (getline(ini_file1, line)) {
			out_file1 << line << "\n";
		}
	}
	ini_file1.close();
	out_file1.close();
	remove(arr);
}
void FileOperations::addAtSpecificLine(string prjName, string content, string name, int no) {

	string substr = prjName.substr(0, (prjName.length() - 2));
	string prjUpdates = name + "/" + substr + "/" + prjName + "Changes";
	string projectPath = name + "/" + substr + "/" + prjName;
	ofstream myfile(projectPath.c_str(), ios::app);
	UpdateToAdd(prjName, content, name, no);
	ofstream myfile1(prjUpdates.c_str(), ios::app);
	string update = "Added new line to " + prjName;
	std::cout << update << std::endl;
	myfile1 << update << "\n";

}



//add a line to the file
void FileOperations::addAtEnd(string prjName, string content, string name) {
	string substr = prjName.substr(0, (prjName.length() - 2));
	string prjUpdates = name + "/" + substr + "/" + prjName + "Changes";
	string projectPath = name + "/" + substr + "/" + prjName;
	ofstream myfile(projectPath.c_str(), ios::app);
	myfile << content << "\n";
	ofstream myfile1(prjUpdates.c_str(), ios::app);
	string update = "Added new line to " + prjName;
	std::cout << update << std::endl;
	myfile1 << update << "\n";
}


void FileOperations::addingContentAndUpdateVersion(int max, string prjName, string content, string name) {
	Validations vp;
	ProjectOperations po;
	FileOperations fo;
	string val = vp.convertToString(max);
	string file = prjName.substr(0, (prjName.length() - 2)) + val;
	string line;
	string substr = prjName.substr(0, (prjName.length() - 2));
	string prjupdate = name + "/" + substr + "/" + file + "Changes";
	string projectPath = name + "/" + substr + "/" + prjName;
	string projectPath1 = name + "/" + substr + "/" + file;
	ifstream ini_file(projectPath.c_str());
	ofstream out_file(projectPath1.c_str(), ios::app);
	if (ini_file.is_open()) {
		while (getline(ini_file, line)) {
			out_file << line << endl;
		}
	}
	ini_file.close();
	out_file << content << endl;
	out_file.close();
	string vall = "Added new line to " + file;
	std::cout << vall << std::endl;
	ofstream myfile(prjupdate.c_str(), ios::app);
	myfile << vall << "\n";
	fo.writeContent(prjName, file, name);
}

void FileOperations::addAtSpecificLineAndUpdateVersion(int max, string prjName, string content, string name, int no) {
	Validations vp;
	ProjectOperations po;
	FileOperations fo;
	string val = vp.convertToString(max);
	string file = prjName.substr(0, (prjName.length() - 2)) + val;
	string line;
	string substr = prjName.substr(0, (prjName.length() - 2));
	string prjupdate = name + "/" + substr + "/" + file + "Changes";
	string projectPath = name + "/" + substr + "/" + prjName;
	string projectPath1 = name + "/" + substr + "/" + file;
	ifstream ini_file(projectPath.c_str());
	ofstream out_file(projectPath1.c_str(), ios::app);
	if (ini_file.is_open()) {
		while (getline(ini_file, line)) {
			out_file << line << endl;
		}
	}
	ini_file.close();
	out_file.close();
	fo.UpdateToAdd(file, content, name, no);
	string vall = "Added new line to " + file;
	std::cout << vall << std::endl;
	ofstream myfile(prjupdate.c_str(), ios::app);
	myfile << vall << "\n";
	fo.writeContent(prjName, file,name);
}



void findMethod(string prjName, string content, string config, int no, string unamel) {
	Validations vp;
	FileOperations fo;
	int max = 0, l = 0;
	string num;
	int m = 0;
	string substr = prjName.substr(0, (prjName.length() - 2));
	string version = unamel + "/" + substr + "/" + substr + "_ projectVersion.txt";
	ifstream newfile(version.c_str(), ios::in);
	if (newfile) {
		num = prjName.substr(prjName.length() - 2);
		max = vp.convertToInt(num);
		std::vector<string> vec;
		if (newfile.is_open()) {
			string tp, sp;
			while (getline(newfile, tp)) {
				if (tp != "") {
					sp = "";
					for (int i = 0; i < tp.size(); i++) {
						if (tp.at(i) != ' ') {
							sp.append(1, tp.at(i));
						}
						else {
							vec.push_back(sp);
							sp = "";
						}
					}
					vec.push_back(sp);
					string ct = vec.at(1);
					int l1 = (ct.length() - 2);
					int l2 = (prjName.length() - 2);
					string v1 = vec.at(1);
					if (ct.substr(0, l1).compare(prjName.substr(0, l2)) == 0) {
						l = vp.convertToInt((vec.at(1)).substr((vec.at(1)).length() - 2));
						if (max < l) {
							max = l;
							m++;
						}
					}
				}
				vec.clear();
			}
		}
	}
	
	if (m > 0) {
		max = max + 1;
		if (no == -1)
			fo.addingContentAndUpdateVersion(max, prjName, content, unamel);
		else
			fo.addAtSpecificLineAndUpdateVersion(max, prjName, content, unamel, no);

	}
	else {
		num = prjName.substr(prjName.length() - 2);
		int maxt = vp.convertToInt(num);
		maxt = maxt + 1;
		if (no == -1)
			fo.addingContentAndUpdateVersion(maxt, prjName, content, unamel);
		else
			fo.addAtSpecificLineAndUpdateVersion(maxt, prjName, content, unamel, no);
	}
}
void FileOperations::add(string prjName, string content, string config, int no, string unamel) {
	Validations vp;
	int conf1 = vp.convertToInt(config);
	int count;
	string substr = prjName.substr(0, (prjName.length() - 2));
	string prjupdates = unamel + "/" + substr + "/" + prjName + "Changes";
	ifstream ifile(prjupdates.c_str());
	if (!(ifile)) {
		if (no == -1)
			addAtEnd(prjName, content, unamel);
		else
			addAtSpecificLine(prjName, content, unamel, no);
	}
	else {
		count = vp.linecount(prjupdates, unamel);
		if (count % conf1 > 0) {
			if (no == -1)
				addAtEnd(prjName, content, unamel);
			else
				addAtSpecificLine(prjName, content, unamel, no);
		}
		else {
			findMethod(prjName, content, config, no, unamel);
		}
	}
}

void FileOperations::addNewContent() {
	ProjectOperations po;
	std::string prj, content, unamel, newprj, validprj, conf;
	char res;
	int no;
	std::cout << "Enter the project name:" << std::endl;
	std::cin >> prj;
	std::cin.ignore();
	
	Validations vp;
	FileOperations op;
	
	unamel = vp.getUser();
	validprj = vp.projectExists(prj, unamel, 1);

	std::cout << "Enter the content to be added:" << std::endl;
	getline(cin, content);
	int cnt = vp.linecount(validprj, unamel);
	std::string num = vp.convertToString(cnt);
	std::cout << "Whether you need to append the content or not? Y/N?" << std::endl;
	std::cin >> res;
	if (res == 'N') {

		int y = 1;
		while (y) {
			std::cout << "Enter the line number from 0 to " + num + " in which the content to be added : " << std::endl;
			std::cin >> no;
			if (no < 0 || no >cnt) {
				std::cout << "Row not found" << std::endl;
			}
			else {
				y = 0;
			}
		}


	}
	else {
		no = -1;
	}
	conf = vp.findConfiguration(unamel, validprj);

	add(validprj, content, conf, no, unamel);

}

//remove
void FileOperations::findToRemove(string prjName, int n, string name) {
	string substr = prjName.substr(0, (prjName.length() - 2));
	string projectPath = name + "/" + substr + "/" + prjName;
	string line;
	int c = 0;
	const char* arr = "myfile.txt";
	ifstream myfiled(projectPath.c_str(), ios::in);
	ofstream delfile("myfile.txt", ios::app);
	if (myfiled) {
		while (getline(myfiled, line))
		{
			if (c != n) {
				delfile << line << endl;
			}
			c++;
		}
	}
	myfiled.close();
	delfile.close();
	ofstream filef(projectPath.c_str(), std::ofstream::out | std::ofstream::trunc);
	ifstream ini_file1("myfile.txt", ios::in);
	ofstream out_file1(projectPath.c_str(), ios::app);
	if (ini_file1.is_open()) {
		while (getline(ini_file1, line)) {
			out_file1 << line << "\n";
		}
	}
	ini_file1.close();
	out_file1.close();
	remove(arr);
}
//remove a line in a file
void FileOperations::removingline(string prjName, int n, string name) {
	
	Validations vp;
	string substr = prjName.substr(0, (prjName.length() - 2));
	string prjUpdates = name + "/" + substr + "/" + prjName + "Changes";
	string projectPath = name + "/" + substr + "/" + prjName;
	findToRemove(prjName, n, name);
	ofstream myfilefd(prjUpdates.c_str(), ios::app);
	string update = "Removed a line from " + prjName;
	std::cout << update << std::endl;
	myfilefd << update << endl;
}
void FileOperations::removinglineAndUpdateVersion(int max, string prjName, int n, string name) {

	Validations vp;
	ProjectOperations po;
	FileOperations fo;
	string val = vp.convertToString(max);
	string file = prjName.substr(0, (prjName.length() - 2)) + val;
	string substr = prjName.substr(0, (prjName.length() - 2));
	string prjupdate = name + "/" + substr + "/" + file + "Changes";
	string projectPath = name + "/" + substr + "/" + prjName;
	string projectPath1 = name + "/" + substr + "/" + file;
	string line;
	ifstream ini_file(projectPath.c_str());
	ofstream out_file(projectPath1.c_str(), ios::app);
	if (ini_file.is_open()) {
		while (getline(ini_file, line)) {
			out_file << line << endl;
		}
	}
	ini_file.close();
	out_file.close();
	fo.findToRemove(file, n, name);
	string vall = "Removed a line from " + file;
	std::cout << vall << std::endl;
	ofstream myfilea(prjupdate.c_str(), ios::app);
	myfilea << vall << "\n";
	fo.writeContent(prjName, file,name);
}

void findMethod1(string prjName, int n, string config, string name) {
	
	Validations vp;
	FileOperations fo;
	int max = 0, l = 0;
	string num;
	int m = 0;
	string substr = prjName.substr(0, (prjName.length() - 2));
	string version = name + "/" + substr + "/" + substr + "_ projectVersion.txt";
	
	ifstream newfile(version.c_str(), ios::in);
	if (newfile) {
		num = prjName.substr(prjName.length() - 2);
		max = vp.convertToInt(num);
		std::vector<string> vec;
		if (newfile.is_open()) {
			string tp, sp;
			while (getline(newfile, tp)) {
				if (tp != "") {
					sp = "";
					for (int i = 0; i < tp.size(); i++) {

						if (tp.at(i) != ' ') {
							sp.append(1, tp.at(i));
						}
						else {
							vec.push_back(sp);
							sp = "";
						}
					}
					vec.push_back(sp);
					string ct = vec.at(1);
					int l1 = (ct.length() - 2);
					int l2 = (prjName.length() - 2);

					if (ct.substr(0, l1) == prjName.substr(0, l2)) {
						l = vp.convertToInt((vec.at(1)).substr((vec.at(1)).length() - 2));
						if (max < l) {
							max = l;
							m++;
						}
					}
					vec.clear();
				}
			}
		}
	}
	if (m > 0) {
		max = max + 1;
		fo.removinglineAndUpdateVersion(max, prjName, n, name);
	}
	else {
		num = prjName.substr(prjName.length() - 2);
		int maxt = vp.convertToInt(num);
		maxt = maxt + 1;
		fo.removinglineAndUpdateVersion(maxt, prjName, n, name);
	}
}
void FileOperations::removeContent(string prjName, int n, string config, string name) {
	Validations vp;
	int conf1 = vp.convertToInt(config);
	int count;
	string substr = prjName.substr(0, (prjName.length() - 2));
	string prjupdates = name + "/" + substr + "/" + prjName + "Changes";
	ifstream ifile(prjupdates.c_str());
	if (!(ifile)) {
		removingline(prjName, n, name);
	}
	else {
		count = vp.linecount(prjupdates, name);
		if (count % conf1 > 0) {
			removingline(prjName, n, name);
		}
		else {
			findMethod1(prjName, n, config, name);
		}
	}
}
void FileOperations::removeAContent() {
	ProjectOperations po;
	string prj, unamel, validprj, conf;
	int removecont;
	std::cout << "Enter the project name:" << std::endl;
	std::cin >> prj;
	std::cin.ignore();
	
	Validations vp;
	FileOperations fo;
	unamel = vp.getUser();
	validprj = vp.projectExists(prj, unamel, 1);
	int y = 1;
	while (y) {
		int cnt = vp.linecount(validprj, unamel);
		std::cout << "Enter any row number :" << std::endl;
		std::cin >> removecont;
		if (removecont < 0 || removecont >= cnt) {
			std::cout << "Row not found" << std::endl;
		}
		else {
			y = 0;
		}
	}
	conf = vp.findConfiguration(unamel, validprj);
	fo.removeContent(validprj, removecont, conf, unamel);

}

//remove whole contents
void FileOperations::removingWholeContent(string prjName, string name) {
	string substr = prjName.substr(0, (prjName.length() - 2));
	string prjUpdates = name + "/" + substr + "/" + prjName + "Changes";
	string projectPath = name + "/" + substr + "/" + prjName;

	ofstream file(projectPath.c_str(), std::ofstream::out | std::ofstream::trunc);
	file.close();
	ofstream myfile(prjUpdates.c_str(), ios::app);
	string update = "Removed whole contents from " + prjName;
	std::cout << update << std::endl;
	myfile << update << endl;
}
void FileOperations::removingWholeAndUpdateVersion(int max, string prjName, string name) {

	Validations vp;
	ProjectOperations po;
	FileOperations fo;
	string val = vp.convertToString(max);
	string file = prjName.substr(0, (prjName.length() - 2)) + val;
	string line;
	string substr = prjName.substr(0, (prjName.length() - 2));
	string prjUpdate = name + "/" + substr + "/" + file + "Changes";
	string projectPath = name + "/" + substr + "/" + prjName;
	string projectPath1 = name + "/" + substr + "/" + file;
	ifstream ini_file(projectPath.c_str());
	ofstream out_file(projectPath1.c_str(), ios::app);
	if (ini_file.is_open()) {
		while (getline(ini_file, line)) {
			out_file << line << endl;
		}
	}
	ini_file.close();
	out_file.close();
	ofstream clearFile(projectPath1.c_str(), std::ofstream::out | std::ofstream::trunc);
	clearFile.close();
	string vall = "Removed whole contents from " + projectPath1;
	std::cout << vall << std::endl;
	ofstream myfilea(prjUpdate.c_str(), ios::app);
	myfilea << vall << "\n";
	fo.writeContent(prjName, file,name);
}

void findMethod4(string prjName, string config, string name) {
	Validations vp;
	FileOperations fo;
	int max = 0, l = 0;
	string num;
	int m = 0;
	string substr = prjName.substr(0, (prjName.length() - 2));
	string version =name + "/" + substr + "/" + substr + "_ projectVersion.txt";
	
	ifstream newfile(version.c_str(), ios::in);
	if (newfile) {
		num = prjName.substr(prjName.length() - 2);
		max = vp.convertToInt(num);
		std::vector<string> vec;
		if (newfile.is_open()) {
			string tp, sp;
			while (getline(newfile, tp)) {
				if (tp != "") {
					sp = "";
					for (int i = 0; i < tp.size(); i++) {

						if (tp.at(i) != ' ') {
							sp.append(1, tp.at(i));
						}
						else {
							vec.push_back(sp);
							sp = "";
						}
					}
					vec.push_back(sp);
					string ct = vec.at(1);
					int l1 = (ct.length() - 2);
					int l2 = (prjName.length() - 2);

					if (ct.substr(0, l1) == prjName.substr(0, l2)) {
						l = vp.convertToInt((vec.at(1)).substr((vec.at(1)).length() - 2));
						if (max < l) {
							max = l;
							m++;
						}
					}
				}
				vec.clear();
			}
		}
	}
	if (m > 0) {
		max = max + 1;
		fo.removingWholeAndUpdateVersion(max, prjName, name);
	}
	else {
		num = prjName.substr(prjName.length() - 2);
		int maxt = vp.convertToInt(num);
		maxt = maxt + 1;
		fo.removingWholeAndUpdateVersion(maxt, prjName, name);
	}
}
void FileOperations::removeWhole(string prjName, string config, string name) {
	Validations vp;
	int conf1 = vp.convertToInt(config);
	int count;
	string substr = prjName.substr(0, (prjName.length() - 2));
	string prjupdates = name + "/" + substr + "/" + prjName + "Changes";

	ifstream ifile(prjupdates.c_str());
	if (!(ifile)) {
		removingWholeContent(prjName, name);
	}
	else {
		count = vp.linecount(prjupdates, "");
		if (count % conf1 > 0) {
			removingWholeContent(prjName, name);
		}
		else {
			findMethod4(prjName, config, name);

		}
	}


}
void FileOperations::removePrjContents() {
	string prj, unamel, validprj, conf;
	ProjectOperations po;
	std::cout << "Enter the project name:" << std::endl;
	std::cin >> prj;
	std::cin.ignore();
	
	Validations vp;
	
	unamel = vp.getUser();
	validprj = vp.projectExists(prj, unamel, 1);
	conf = vp.findConfiguration(unamel, validprj);
	removeWhole(validprj, conf, unamel);
}









