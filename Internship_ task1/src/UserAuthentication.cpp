#include "Header.h"
#include "ClassHeaders.h"

using namespace std;

bool IsPathExist(const std::string& s)
{
	struct stat buffer;
	return (stat(s.c_str(), &buffer) == 0);
}

string getPhonenumber() {
	Validations vp;
	string phn, reEnterPhn1, reEnterPhn;
	std::cout << "Enter phonenumber :" << std::endl;
	std::cin >> phn;
	reEnterPhn1 = vp.PhoneChecker(phn);
	ifstream ifile("userdata.txt");
	if (ifile) {
		string str = "Phone number";
		reEnterPhn = vp.userExists(reEnterPhn1, 5, str);
	}
	else {
		reEnterPhn = reEnterPhn1;
	}
	return reEnterPhn;
}

string getMail() {
	Validations vp;
	string email, reEnterUsername, reEnterUsername1;
	std::cout << "Enter email :" << std::endl;
	std::cin >> email;
	reEnterUsername1 = vp.MailChecker(email);
	ifstream ifile("userdata.txt");
	if (ifile) {
		string str = "username";
		reEnterUsername = vp.userExists(reEnterUsername1, 3, str);
	}
	else {
		reEnterUsername = reEnterUsername1;
	}
	return reEnterUsername;

}

string getDOB() {
	Validations vp;
	int day, month, year, j = 1;
	string DOB;
	while (j) {
		std::cout << "Enter dob in the format of (day month year):" << std::endl;
		std::cin >> day >> month >> year;
		if (vp.validDOB(day, month, year)) {
			j = 0;
		}
		else {
			std::cout << "Invalid date" << std::endl;
		}
	}
	string day1 = vp.convertToString(day);

	string month1 = vp.convertToString(month);

	string year1 = vp.convertToString(year);

	DOB = day1 + "-" + month1 + "-" + year1;
	return DOB;

}

void goToSignUp() {
	std::cout << "Please Signup:" << std::endl;
	string reEnterPass, reEnterPhn, reEnterPassword1, password1, password2, result;
	string userName, location, favPlace, pwd, currentPwd,dob,mail;
	Validations vp;
	std::cout << "Enter username :" << std::endl;
	std::cin >> userName;
	dob=getDOB();
	mail=getMail();
	std::cout << "Enter location :" << std::endl;
	std::cin >> location;
	reEnterPhn=getPhonenumber();
	std::cout << "Enter your favourite place :" << std::endl;
	std::cin >> favPlace;
	int cc = 1;
	while (cc == 1) {
		std::cout << "Enter password :" << std::endl;
		std::cin >> pwd;
		reEnterPass = vp.validate(pwd);
		password1 = vp.encrypt(reEnterPass);
		std::cout << "Confirm password :" << std::endl;
		std::cin >> currentPwd;
		reEnterPassword1 = vp.validate(currentPwd);
		password2 = vp.encrypt(reEnterPassword1);
		if (reEnterPass == reEnterPassword1) {
			cc = 0;
		}
		else {
			cc = 1;
			std::cout << "password mismatch" << std::endl;
		}
	}
	int count = vp.linecount("userdata.txt","");
	string strr = vp.convertToString(count);
	

	result = strr + " " + userName + " " + dob + " " + mail + " " + location + " " + reEnterPhn + " " + password1 + " " + favPlace;
	
	ofstream myfile;
	myfile.open("userdata.txt",ios::app);
	myfile << result << "\n";
	myfile.close();
	std::cout << "Successfully signed up" << std::endl;
	UserAuthentication up;
	up.login();
}

void FetchingUserDetails() {

	Validations vp;
	string user1Name, pswd1, pass1, login;
	char res, option;
	int v1 = 0, v2 = 0, v3 = 0;
	while (v2 == 0) {
		std::cout << "Enter your email id :" << std::endl;
		std::cin >> user1Name;
		user1Name = vp.MailChecker(user1Name);
		ifstream newfile("userdata.txt");
		string reval;
		int cnt = 0;

		if (newfile.is_open()) {
			string line, sp;

			while (getline(newfile, line)) {
				stringstream ss(line);
				while (getline(ss, sp, ' ')) {
					if (sp == user1Name) {
						cnt = 1;
						v2 = 1;
					}
					if (cnt == 1) {
						v3++;

						if (v3 == 4) {

							while (v1 == 0) {
								std::cout << "Enter password :" << std::endl;
								std::cin >> pswd1;
								pass1 = vp.encrypt(pswd1);
								if (sp == pass1) {
									v1 = 1;

								}
								else {
									std::cout << "invalid password" << std::endl;
									std::cout << "Is password resetting required? Y/N?" << std::endl;
									std::cin >> res;
									if (res == 'Y') {
										vp.resetPassword(user1Name);
										v1 = 1;
									}
								}
							}
							break;
						}

					}
				}
				if (cnt == 1 && v1 == 1) {
					break;
				}

			}

			if (cnt == 0) {
				std::cout << "Username not found" << std::endl;
				std::cout << "Whether you need to sign up or not? Y/N?" << std::endl;
				std::cin >> option;
				cin.ignore();
				if (option == 'Y') {
					goToSignUp();
					v2 = 1;
				}

			}
		}
		else {
			std::cout << "username not found" << std::endl;
			goToSignUp();
			v2 = 1;
		}
	}
	int cnt1 = vp.linecount("logindata.txt","");
	string strr;
	stringstream ss;
	ss << cnt1;
	ss >> strr;
	login = strr + " " + user1Name + " " + pass1;
	ofstream myfile1("logindata.txt", ios::app);
	myfile1 << login << "\n";
	myfile1.close();
	std::cout << "Logged in successfully" << std::endl;
	string path = user1Name;
	if (!IsPathExist(path)) {
		_mkdir(path.c_str());
	}
	StartOperations so;
	so.ini_Operations(user1Name);
  
}

void UserAuthentication:: login() {
	std::cout << "Welcome to Project portal!"<<std::endl;
	std::cout << "Kindly Login to continue" << std::endl;
	FetchingUserDetails();
}
