//works in c++ 14

#include<iostream>
#include<thread>
static bool finish=false;
void doWork(){
    using namespace std::chrono_literals;
	std::cout<<"Started thread id = "<<std::this_thread::get_id()<<std::endl;
	while(!finish){
		std::cout<<"Working"<<std::endl;
		std::this_thread::sleep_for(1s);
	}
}
int main(){
	std::thread worker(doWork);
	std::cin.get();
	finish=true;
	worker.join();
	std::cout<<"function ended"<<std::endl;
	std::cout<<"Started thread id = "<<std::this_thread::get_id()<<std::endl;
	std::cin.get();
}


