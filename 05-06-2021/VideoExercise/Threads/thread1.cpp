#include<iostream>
#include<thread>
static bool finish=false;
void doWork(){
	while(!finish){
		std::cout<<"Working"<<std::endl;
	}
}
int main(){
	std::thread worker(doWork);
	std::cin.get();
	finish=true;
	worker.join();
	std::cout<<"function ended"<<std::endl;
	std::cin.get();
}
