#include<iostream>
#include<vector>
void ForEach(const std::vector<int>& val,void(*func)(int)){
    for(int values : val)
        func(values);
}
int main(){
    std::vector<int> val={1,2,3,4,5};
    ForEach(val,[](int val1){
        std::cout<<"value : "<<val1<<std::endl;
    });
}
            
