#include<iostream>
#include<vector>
#include<functional>
void ForEach(const std::vector<int>& val,const std::function<void(int)>& func){
    for(int values : val)
        func(values);
}
int main(){
    std::vector<int> val={1,2,3,4,5};
    int a=5;
    auto lambda=[=](int val1) mutable{
        a=56;
         std::cout<<"value : "<<val1<<std::endl;
    };
    ForEach(val,lambda);
}
            
