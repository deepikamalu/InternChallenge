#include<iostream>
#include<string>
class Entity{
	public:
		virtual void print(){
			std::cout<<"Hi"<<std::endl;
		}
};
class Player:public Entity{
	public:
		void print(){
			std::cout<<"Hello"<<std::endl;
		}
};
class Enemy:public Entity{
	public:
		void print(){
			std::cout<<"fine"<<std::endl;
		}
};
int main(){
	Player* p=new Player();
	p->print();
	Entity* e1=new Enemy();
	e1->print();
	Entity* p1=p;
	p1->print();
	Player* p2=dynamic_cast<Player*> (p1);	
	p2->print();
//	Player* p3=dynamic_cast<Player*> (e1);	
//	p3->print();
}
