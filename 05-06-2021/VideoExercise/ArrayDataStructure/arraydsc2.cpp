#include<iostream>
#include<array>
template<typename T,size_t S>
class Array{
    public:
       constexpr size_t Size() const{
           return S;
       }
       T& operator[](size_t index){
           return m_data[index];
       }
       T* Data(){
           return m_data;
       }
    private:
       T m_data[S];
};
int main(){
    Array<int,4>data;
    for(size_t i=0;i<data.Size();i++){
        data[i]=i;
    }
     for(size_t i=0;i<data.Size();i++){
        std::cout<<data[i]<<std::endl;
    }
    std::cin.get();
}
