#include<iostream>
#include<vector>
void print(int val){
    std::cout<<val<<std::endl;
}
void ForEach(const std::vector<int>& val1,void(*func)(int)){
    for(int i:val1){
        func(i);
    }
}
int main(){
    std::vector<int> val={1,2,3,4,5};
    ForEach(val,print);
}
