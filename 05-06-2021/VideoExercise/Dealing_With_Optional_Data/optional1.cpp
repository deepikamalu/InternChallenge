#include<iostream>
#include<string>
#include<optional>
#include<fstream>
std::optional<std::string> readfile(const std::string& path){
    std::ifstream stream(path);
    if(stream){
        std::string result="success";
        stream.close();
        return result;
    }
    return {};
}
int main(){
    std::optional<std::string>data=readfile("file.txt");
    std::string val=data.value_or("Not found");   
    std::cout<<val<<std::endl;
    if(data.has_value()){  //data.has_value() (or) data
        std::cout<<"File read successfully"<<std::endl;
    }
    else{
        std::cout<<"File could not be opened"<<std::endl;
    }
    std::cin.get();
}
