#include<iostream>
#include<variant>
#include<string>
int main(){
	std::variant<std::string,int> data;
    std::cout<<sizeof(int)<<"\n";
    std::cout<<sizeof(std::string)<<"\n";
    std::cout<<sizeof(data)<<"\n";
	data="Deepika";
	std::cout<<std::get<std::string> (data)<<"\n";
	data=20;
	std::cout<<std::get<int> (data)<<"\n";
	std::cin.get();
}
