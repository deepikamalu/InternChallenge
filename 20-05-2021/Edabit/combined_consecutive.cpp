#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;
bool consecutiveCombo(vector<int> a1, vector<int> a2) {
	int n=a1.size()+a2.size();
	int arr[n],k=0,c=0;
	for(int i=0;i<a1.size();i++){
		arr[k++]=a1.at(i);
	}
	for(int i=0;i<a2.size();i++){
		arr[k++]=a2.at(i);
	}
	sort(arr,arr+n);
	for(int i=0;i<n-1;i++){
		if(arr[i+1]!=arr[i]+1){
			c=1;
			break;
		}
	}
	if(c!=1){
		return true;
	}
	else{
		return false;
	}
}
int main(){
	vector<int> v1,v2;
	int temp,n1,n2;
	std::cout<<"Enter n1"<<std::endl;
	std::cin>>n1;
	std::cout<<"Enter n2"<<std::endl;
	std::cin>>n2;
	std::cout<<"Enter the values of arr1:"<<std::endl;
	for(int i=0;i<n1;i++){
	    std::cin>>temp;	
	    v1.push_back(temp);
	}
	std::cout<<"Enter the values of arr2:"<<std::endl;
	for(int i=0;i<n2;i++){
	    std::cin>>temp;	
	    v2.push_back(temp);
	}
	if(consecutiveCombo(v1,v2)){
	std::cout<<"true"<<std::endl;		
	}
	else{
	std::cout<<"false"<<std::endl;		
	}
}
