#include<iostream>
#include<string>
#include<cmath>
using namespace std;
int minRemovals(string str1, string str2) {
	int arr1[26]={0};
	int arr2[26]={0};
	for(int i=0;i<str1.length();i++){
		arr1[str1.at(i)-'a']++;
	}
	for(int i=0;i<str2.length();i++){
		arr2[str2.at(i)-'a']++;
	}
	int sum=0;
	for(int i=0;i<26;i++){
		sum+=abs(arr1[i]-arr2[i]);
	}
	return sum;
}
int main(){
	string str1,str2;
	std::cout<<"Enter the strings:"<<std::endl;
	std::cin>>str1;
	std::cin>>str2;
	std::cout<<minRemovals(str1,str2)<<std::endl;
}
