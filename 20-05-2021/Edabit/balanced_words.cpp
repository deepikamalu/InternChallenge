#include<iostream>
#include<string>
#include<algorithm>
using namespace std;
bool balanced(string word) {
	int ed,c1=0,c2=0;
	if(word.length()%2==0){
		ed=word.length()/2;
	}
	else{
		ed=(word.length()/2)+1;
	}
	for(int i=0;i<word.length()/2;i++){
		c1+=word.at(i)-'a';
	}
	for(int i=ed;i<word.length();i++){
		c2+=word.at(i)-'a';
	}
	string str2=word;
	reverse(word.begin(),word.end());
	if(c1==c2 || str2.compare(word)==0){
		return true;
	}
	else{
		return false;
	}
}
int main(){
	string word;
	std::cout<<"Enter the string:"<<std::endl;
	std::cin>>word;
	if(balanced(word)){
		std::cout<<"true"<<std::endl;
	}
	else{
		std::cout<<"false"<<std::endl;
	}
}
