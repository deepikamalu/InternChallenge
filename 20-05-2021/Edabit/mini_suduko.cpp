#include<iostream>
#include<vector>
bool isMiniSudoku(std::vector<std::vector<int> > square) {
	int arr[10]={0};
	for(int i=0;i<square.size();i++){
		for(int j=0;j<square[i].size();j++){
			arr[square[i][j]]++;
	    }
    }
    for(int i=1;i<=9;i++){
    	if(arr[i]!=1){
    		return false; 
		}
	}
	return true;
}
int main(){
	int row,col,temp;
	std::cout<<"Enter the no of rows:"<<std::endl;
	std::cin>>row;
	std::cout<<"Enter the no of cols:"<<std::endl;
	std::cin>>col;
	std::vector<std::vector<int> > vec;
	std::cout<<"Enter the values:"<<std::endl;
    for (int i = 0; i < row; i++) {
        std::vector<int> v1;
        for (int j = 0; j < col; j++) {
        	std::cin>>temp;
            v1.push_back(temp);
        }
        vec.push_back(v1);
    }
    if(isMiniSudoku(vec)){
    	std::cout<<"true"<<std::endl;
	}
	else{
	    std::cout<<"false"<<std::endl;
	}
    
}
