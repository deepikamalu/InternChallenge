#include<iostream>
class Log{
	public:
		const int LogLevelError=0;
		const int LogLevelWarning=1;
		const int LogLevelInfo=2;
	private:
		int m_logLevel=LogLevelInfo;
	public:
	    void setLevel(int level){
	    	m_logLevel=level;
		}	
		void LevelError(const char* msg){
			if(m_logLevel>=LogLevelError){
				std::cout<<"[ERROR]:"<<msg<<std::endl;
			}
		}
		void LevelInfo(const char* msg){
			if(m_logLevel>=LogLevelInfo){
				std::cout<<"[INFO]:"<<msg<<std::endl;
			}
		}
		void LevelWARNING(const char* msg){
			if(m_logLevel>=LogLevelWarning){
				std::cout<<"[WARNING]:"<<msg<<std::endl;
			}
		} 	
};
int main(){
	Log log;
	log.setLevel(log.LogLevelWarning);
	log.LevelError("Hi");
	log.LevelInfo("Hi");
	log.LevelWARNING("Hi");
	std::cin.get();
}
