#include<iostream>
#include<string>
using namespace std;
class Employee{
	public:
	    string name;
	    string id;
	    Employee(){
		    name="none";
		    id="none";
	    }
	    Employee(string empname,string empid){
		    name=empname;
		    id=empid;
	    }
	    void display(){
		    std::cout<<"Employee name : "<<name<<", id : "<<id<<std::endl;
	    }
};
int main(){
	Employee emp;
	emp.display();
	string nm1,id1;
	std::cout<<"Enter name:"<<std::endl;
	getline(cin,nm1);
	std::cout<<"Enter id:"<<std::endl;
	std::cin>>id1;
	Employee emp1(nm1,id1);
	emp1.display();
}
