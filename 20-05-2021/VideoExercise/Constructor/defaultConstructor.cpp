#include<iostream>
#include<string>
using namespace std;
class Employee{
	public:
	    string name;
	    string id;
	    Employee(){
		    name="none";
		    id="none";
	    }
	    void display(){
		    std::cout<<"Employee name : "<<name<<", id : "<<id<<std::endl;
	    }
};
int main(){
	Employee emp;
	emp.display();
}
