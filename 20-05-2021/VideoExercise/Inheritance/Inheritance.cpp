#include<iostream>
class Entity{
	public:
		int x;
		int y;
		void display(int xa,int yb){
			x=xa;
			y=yb;
			std::cout<<x<<","<<y<<std::endl;
		}
};
class Player : public Entity{
    public:
        double speed;
        void displaySpeed(int spd){
        	speed=spd;
        	std::cout<<spd<<std::endl;
		}
	
};
int main(){
	Entity ent;
	Player ply;
	ply.display(2,3);
	ply.displaySpeed(20);
	ent.display(5,8);
	std::cout<<"Entity size : "<<sizeof(Entity)<<std::endl;
	std::cout<<"Player sze : "<<sizeof(Player)<<std::endl;
}
