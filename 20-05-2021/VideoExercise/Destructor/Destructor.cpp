#include<iostream>
#include<string>
using namespace std;
class Employee{
	public:
	    string name;
	    string id;
	    Employee(){
	    	std::cout<<"Constructor created"<<std::endl;
		    name="none";
		    id="none";
	    }
	    ~Employee(){
	    	std::cout<<"Destroyed constructor"<<std::endl;
		}
	    void display(){
		    std::cout<<"Employee name : "<<name<<", id : "<<id<<std::endl;
	    }
};
int main(){
	Employee emp;
	emp.display();
}
