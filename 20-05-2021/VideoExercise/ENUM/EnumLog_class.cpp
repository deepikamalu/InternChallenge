#include<iostream>
class Log{
	public:
		enum Level{
		LogLevelError,
		LogLevelWarning,
		LogLevelInfo,
		};
		
	private:
		Level m_logLevel=LogLevelInfo;
	public:
	    void setLevel(Level level){
	    	m_logLevel=level;
		}	
		void LevelError(const char* msg){
			if(m_logLevel>=LogLevelError){
				std::cout<<"[ERROR]:"<<msg<<std::endl;
			}
		}
		void LevelInfo(const char* msg){
			if(m_logLevel>=LogLevelInfo){
				std::cout<<"[INFO]:"<<msg<<std::endl;
			}
		}
		void LevelWARNING(const char* msg){
			if(m_logLevel>=LogLevelWarning){
				std::cout<<"[WARNING]:"<<msg<<std::endl;
			}
		} 	
};
int main(){
	Log log;
	log.setLevel(Log::LogLevelWarning);
	log.LevelError("Hi");
	log.LevelInfo("Hi");
	log.LevelWARNING("Hi");
	std::cin.get();
}
