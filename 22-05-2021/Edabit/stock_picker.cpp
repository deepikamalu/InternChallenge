#include<iostream>
#include<vector>
int stockPicker(std::vector<int> arr) {
	int max=0,c,c1=0;
	for(int i=0;i<arr.size();i++){
		c=0;
		for(int j=i+1;j<arr.size();j++){
			if(arr.at(i)<arr.at(j)){
				if(max<(arr.at(j)-arr.at(i))){
					max=arr.at(j)-arr.at(i);
					c1=1;
				}
			}
		}
	}
	if(c1!=0){
		return max;
	}
	else{
		return -1;
	}
}
int main(){
	int n,t;
	std::vector<int> vec;
	std::cout<<"Enter the value of n:"<<std::endl;
	std::cin>>n;
	std::cout<<"Enter the values:"<<std::endl;
	for(int i=0;i<n;i++){
		std::cin>>t;
		vec.push_back(t);
	}
	std::cout<<stockPicker(vec)<<std::endl;
}
