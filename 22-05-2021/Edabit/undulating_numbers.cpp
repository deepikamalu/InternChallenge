#include<iostream>
#include<algorithm>
using namespace std;
int find(int num){
	int c=0;
	while(num!=0){
		c++;
		num=num/10;
	}
	return c;
}
bool isUndulating(int n) {
    int c=find(n);
    int arr[c],k=0;
    while(n!=0){
    	arr[k++]=n%10;
    	n=n/10;
	}
	int c1=0,c2=0;
	if(arr[c-1]!=arr[c-2]){
	    for(int i=c-1;i>=0;i--){
		    if(arr[i]==arr[c-1]){
			   c1++;
		    }
		    else if(arr[i]==arr[c-2]){
			   c2++;
		    }
	        }
    }
    else{
    	return false;
	}
	if(c>=3 && c1!=0 && c2!=0){
	    if(c%2==0){
		    if(c1==c2 && (c1+c2)==c){
			    return true;
		    }
		    else{
			    return false;
		    } 
	    }
	    else{
		    if(c1==c2+1 && (c1+c2)==c){
			    return true;
		    } 
		    else{
			    return false;
		    } 
	    }
    }
    else{
    	return false;
	}
}
int main(){
	int num;
	std::cout<<"Enter the number:"<<std::endl;
	std::cin>>num;
	isUndulating(num) ? std::cout<<"true"<<std::endl : std::cout<<"false"<<std::endl;
}
