#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;
int palindrome(string str){
	string str1,str2,str3,str4;
	int c1,c2;
	for(int i=0;i<str.length();i++){
		if(str.at(i)>='a' && str.at(i)<='z'){
			str1.append(1,str.at(i));
		}
		else{
			str2.append(1,str.at(i));
		}
	}
	str3=str1;
	str4=str2;
	reverse(str3.begin(),str3.end());
	reverse(str4.begin(),str4.end());
	if(str.length()!=0 || str2.length()!=0){
	    if(str1.length()==1 && str2.length()==1){
		    return 2;
	    }
	    else if(str1.length()==0 && str2.length()!=0){
		    if(str2.compare(str4)==0){
			    return 1;
		    }
		    else{
			    return 0;
		    }
	    }
	    else if(str1.length()!=0 && str2.length()==0){
	        if(str1.compare(str3)==0){
			    return 1;
		    }
		    else{
			    return 0;
		    }
	    }
	    else{
	    	if((str2.compare(str4)==0) && (str1.compare(str3)==0)){
	    		return 2;
			}
			else if((str2.compare(str4)==0 )|| (str1.compare(str3)==0)){
				return 1;
			}
			else {
				return 0;
			}
		}
	}
	else{
		return 0;
	}
}
std::vector<int> palindromeSet(std::vector<std::string> arr) {
	string str;
	vector<int> vec;
	for(int i=0;i<arr.size();i++){
		vec.push_back(palindrome(arr.at(i)));
	}
	return vec;
}
int main(){
	int n;
	string t;
	std::vector<string> vec;
	std::cout<<"Enter the value of n:"<<std::endl;
	std::cin>>n;
	std::cout<<"Enter the values:"<<std::endl;
	for(int i=0;i<n;i++){
		std::cin>>t;
		vec.push_back(t);
	}
	vector<int> vec1=palindromeSet(vec);
	for(int i=0;i<n;i++){
	    std::cout<<vec1.at(i)<<" ";
	}
}
