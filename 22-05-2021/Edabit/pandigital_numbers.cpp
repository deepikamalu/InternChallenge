#include<iostream>
bool isPandigital(long long num) {
    int arr[10]={0},rem;
    while(num!=0){
    	rem=num%10;
    	arr[rem]=1;
    	num=num/10;
	}
	for(int i=0;i<10;i++){
		if(arr[i]==0){
			return false;
		}
	}
	return true;
}
int main(){
	long long num;
	std::cout<<"Enter the number:"<<std::endl;
	std::cin>>num;
	if(isPandigital(num)){
		std::cout<<"true"<<std::endl;
	}
	else{
     	std::cout<<"false"<<std::endl;
}
}   
