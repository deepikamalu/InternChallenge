#include<iostream>
#include<string>
using namespace std;
class Entity{
	private:
		string mname;
		int age;
	public:
	    Entity(const string& name)
		    :mname(name),age(-1){
			}
		Entity(int age1)
		    :mname("Unknown"),age(age1){
			}   
		string getname() const{
			return mname;
		}    
		int getage() const{
			return age;
		}  		 	
};
int main(){
	Entity e1(22);
	std::cout<<e1.getname()<<std::endl;
	std::cout<<e1.getage()<<std::endl;
	Entity e2("Cherno");
	std::cout<<e2.getname()<<std::endl;
	std::cout<<e2.getage()<<std::endl;
	
}
