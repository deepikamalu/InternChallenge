#include<iostream>
int main(){
	const char* name1="Cherno";
	//name1[2]="a";--->error
	char* name2="Jack";
	//name2[2]="e";--->error
	char name3[5]={'R','O','S','E'};
	name3[2]='E';
	std::cout<<name1<<std::endl;
	std::cout<<name2<<std::endl;
	std::cout<<name3<<std::endl;
}
