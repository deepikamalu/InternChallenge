#include<iostream>
class Example{
	public:
		Example(){
			std::cout<<"Created Entity"<<std::endl;
		}
		Example(int x){
			std::cout<<"Created Entity "<<x<<"!"<<std::endl;
		}
};
class Entity{
	private:
		int a,b;
		Example exp;
	public:
	    Entity()
		    :a(0),b(0),exp(8){}
		Entity(int A,int B)
		    :a(A),b(B){}
		int getA() const{
			return a;
		}    
		int getB() const{
			return b;
		}  
};
int main(){
	Entity* e1=new Entity();
	std::cout<<e1->getA()<<std::endl;
	std::cout<<(*e1).getB()<<std::endl;
	Entity* e2=new Entity(1,5);
	std::cout<<e2->getA()<<std::endl;
	std::cout<<(*e2).getB()<<std::endl;
	delete e1;
	delete e2;
}
