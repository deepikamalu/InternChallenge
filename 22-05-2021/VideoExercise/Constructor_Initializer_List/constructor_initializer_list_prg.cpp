#include<iostream>
class Example{
	public:
		Example(){
			std::cout<<"Created Entity"<<std::endl;
		}
		Example(int x){
			std::cout<<"Created Entity "<<x<<"!"<<std::endl;
		}
};
class Entity{
	private:
		int a,b;
		Example exp;
	public:
	    Entity()
		    :a(0),b(0),exp(8){}
		Entity(int A,int B)
		    :a(A),b(B){}
		int getA() const{
			return a;
		}    
		int getB() const{
			return b;
		}  
};
int main(){
	Entity e1;
}
