#include<iostream>
class Entity{
	private:
		int a,b;
	public:
	    Entity()
		    :a(0),b(0){}
		Entity(int A,int B)
		    :a(A),b(B){}
		int getA() const{
			return a;
		}    
		int getB() const{
			return b;
		}  
};
int main(){
	Entity e1;
	std::cout<<e1.getA()<<std::endl;
	std::cout<<e1.getB()<<std::endl;
	Entity e2(1,5);
	std::cout<<e2.getA()<<std::endl;
	std::cout<<e2.getB()<<std::endl;	
}
