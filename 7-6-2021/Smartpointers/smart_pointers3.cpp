#include<iostream>
#include<string>
#include<memory>
class Entity{
    public:
    Entity(){
        std::cout<<"Created Entity"<<std::endl;
    }
    ~Entity(){
        std::cout<<"Destroyed Entity"<<std::endl;
    }
    void print(){
        std::cout<<"Hi C++"<<std::endl;
    }
};
int main(){
    std::weak_ptr<Entity> e1;
    {
    std::shared_ptr<Entity> entity=std::make_shared<Entity>();
    entity->print();
    e1=entity;
    }
}
