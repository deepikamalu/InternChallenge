#include<iostream>
#include<chrono>
#include<array>
#include<memory>
class Timer{
    public:
    Timer(){
        m_start=std::chrono::high_resolution_clock::now();
    }
    ~Timer(){
        stop();
    }
    void stop(){
        auto m_end=std::chrono::high_resolution_clock::now();
        auto start=std::chrono::time_point_cast<std::chrono::microseconds>(m_start).time_since_epoch().count();
        auto end=std::chrono::time_point_cast<std::chrono::microseconds>(m_end).time_since_epoch().count();
        auto duration=end-start;
        double ms=duration*0.001;
        std::cout<<duration<<"us("<<ms<<"ms)\n";
    }
    private:
    std::chrono::time_point<std::chrono::high_resolution_clock> m_start;
};
int main(){
    struct Vector2{
        float x,y;
    };
    std::cout<<"Make shared\n";
    {
        std::array<std::shared_ptr<Vector2>,1000> sharedptr;
        Timer timer;
        for(int i=0;i<sharedptr.size();i++)
            sharedptr[i]=std::make_shared<Vector2>();
    }
    std::cout<<"New shared\n";
    {
        std::array<std::shared_ptr<Vector2>,1000> sharedptr;
        Timer timer;
        for(int i=0;i<sharedptr.size();i++)
            sharedptr[i]=std::shared_ptr<Vector2>(new Vector2());
    }
   
}
    
