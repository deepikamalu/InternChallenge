#include<iostream>
#include<chrono>
class Timer{
    public:
    Timer(){
        m_start=std::chrono::high_resolution_clock::now();
    }
    ~Timer(){
        stop();
    }
    void stop(){
        auto m_end=std::chrono::high_resolution_clock::now();
        auto start=std::chrono::time_point_cast<std::chrono::microseconds>(m_start).time_since_epoch().count();
        auto end=std::chrono::time_point_cast<std::chrono::microseconds>(m_end).time_since_epoch().count();
        auto duration=end-start;
        double ms=duration*0.001;
        std::cout<<duration<<"us("<<ms<<"ms)\n";
    }
    private:
    std::chrono::time_point<std::chrono::high_resolution_clock> m_start;
};
int main(){
    int val=0;
    {
        Timer timer;
        for(int i=0;i<1000000;i++)
            val+=2;
        std::cout<<val<<std::endl;
    }
}
    
