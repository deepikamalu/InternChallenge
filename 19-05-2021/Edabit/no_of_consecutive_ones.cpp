#include<iostream>
#include <vector>
int countOnes(std::vector<int> arr) {
	int c=0;
	for(int i=0;i<arr.size()-1;i++){
		if(i==0){
			if(arr.at(i)==1 && arr.at(i+1)==1){
				c++;
			}
		}
		else{
			if(arr.at(i-1)==0 && arr.at(i)==1 && arr.at(i+1)==1){
				c++;
			}
		}
	}
	return c;
}
int main(){
	int temp,n;
	std::vector<int> arr;
	std::cout<<"enter n:"<<std::endl;	
	std::cin>>n;
    std::cout<<"enter values:"<<std::endl;
	for(int i=0;i<n;i++){
		std::cin>>temp;
		arr.push_back(temp);
	}
	int res=countOnes(arr);
	std::cout<<res<<std::endl;
}
