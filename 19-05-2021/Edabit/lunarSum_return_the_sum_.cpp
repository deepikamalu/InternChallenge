#include<iostream>
int max(int a,int b){
	return (a>b)?a:b;
}
int reverse(int n){
	int r,s=0;
	while(n!=0){
		r=n%10;
		s=s*10+r;
		n=n/10;
	}
	return s;
}
int lunarSum(int a,int b){
	int r1,r2,num=0;
	while(a!=0 && b!=0){
		r1=a%10;
		r2=b%10;
	    num=num*10+max(r1,r2);
	    a=a/10;
	    b=b/10;
	}
	return reverse(num);
}
int main(){
	int n1,n2;
	std::cout<<"Enter two numbers:"<<std::endl;
	std::cin>>n1;
	std::cin>>n2;
	std::cout<<lunarSum(n1,n2)<<std::endl;
}
