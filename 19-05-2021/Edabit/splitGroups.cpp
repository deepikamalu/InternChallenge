#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
using namespace std;
std::vector<std::string> splitGroups(string s) {
	std::vector<string> vec;
	int num;
	for(int i=0;i<s.length();){
		num=count(s.begin(),s.end(),s.at(i));
		vec.push_back(s.substr(i,num));
		
		i=i+num;
	}
	return vec;
}
int main(){
	string str;
	std::cout<<"Enter the string:"<<std::endl;
	std::cin>>str;
	vector<string> vec1;
	vec1=splitGroups(str);
	for(int i=0;i<vec1.size();i++){
		std::cout<<vec1.at(i)<<std::endl;
	}
}
