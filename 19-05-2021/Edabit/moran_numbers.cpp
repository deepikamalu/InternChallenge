#include<iostream>
#include<string>
using namespace std;
bool prime(int n){
	int c=0;
	for(int i=1;i<=n/2;i++){
		if(n%i==0){
			c++;
		}
	}
	if(c==1){
		return true;
	}
	else{
		return false;
	}
}
int digit(int n){
	int sum=0,rem;
	while(n!=0){
    rem=n%10;
	  sum+=rem;
	  n=n/10;
  }
  return sum;
}
string moran(int n) {
	std::string str;
	int sum=digit(n);
	if(prime(n/sum)){
		str.append(1,'M');
	}
	else if(n%sum==0){
		str.append(1,'H');
	} 
	else{
		str+="Neither";
	}
	return str;
}
int main(){
	int n;
	std::cout<<"Enter n:"<<std::endl;
	std::cin>>n;
	std::cout<<moran(n)<<std::endl;
	
}
