#include<iostream>
#include<string>
using namespace std;
string mysteryFunc(string s) {
	int n=s.length()/2;
	int arr[n],k=0;
	string str;
	for(int i=0;i<s.length();i++){
		if(i%2!=0){
			arr[k]=(int)(s.at(i))-48;
			k++;
		}
		else{
			str.append(1,s.at(i));
		}
	}
	string str1;
	for(int i=0,k=0;i<n && k<n;i++,k++){
		for(int j=0;j<arr[k];j++){
			str1.append(1,str.at(i));
		}
	}
	return str1;
}
int main(){
	string str;
	std::cout<<"Enter the string:"<<std::endl;
	std::cin>>str;
	string val=mysteryFunc(str);
	std::cout<<val<<std::endl;
}
