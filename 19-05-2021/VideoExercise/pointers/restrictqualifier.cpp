#include<iostream>
int main(){
	int a=5,b=30;
	int *restrict p1;//here it shows error as it will work only in C99 & C11
	int *restrict p2;
	p1=&a;
	p2=&b;
	//p1=p2--->error(here we cannot assign p1 to p2)
	std::cout<<*p1<<std::endl;
	std::cout<<*p2<<std::endl;
}
