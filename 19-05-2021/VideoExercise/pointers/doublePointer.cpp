#include<iostream>
int main(){
	int a=1,*ptr,**qtr;
	ptr=&a;
	qtr=&ptr;
	std::cout<<"a value:"<<std::endl;
	std::cout<<a<<std::endl;
	std::cout<<"\n*ptr value:"<<std::endl;
	std::cout<<*ptr<<std::endl;
	std::cout<<"\n&ptr value:"<<std::endl;
	std::cout<<&ptr<<std::endl;
	std::cout<<"\n*qtr value:"<<std::endl;
	std::cout<<*qtr<<std::endl;
	std::cout<<"\n**qtr value:"<<std::endl;
	std::cout<<**qtr<<std::endl;
}

