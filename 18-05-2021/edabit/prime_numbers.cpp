#include<iostream>
bool prime(int val){
  int cnt=0;
  for(int i=2;i<=(val/2);i++){
    if(val%i==0){
      cnt++;
    }
  }
  if(cnt==0){
    return true;
  }
  else{
    return false;
  }
}
int primeNumbers(int num) {
  int count=0;
  for(int i=2;i<=num;i++){
    if(prime(i)){
      count++;
    }
  }
  return count;
}
int main()
{
	int n;
	std::cout<<"Enter n:"<<std::endl;
	std::cin>>n;
	std::cout<<primeNumbers(n)<<std::endl;
}
