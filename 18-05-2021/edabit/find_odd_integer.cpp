#include<iostream>
#include <vector>
using namespace std;
int containsVal(int exist[],int val,int n){
	int flag=0;
	for(int i=0;i<n;i++){
		if(val==exist[i])
		{
			flag=1;
			break;
		}
	}
	if(flag==0){
		return 1;
	}
	else{
		return 0;
	}
}
int findOdd(vector<int> arr) {
	int count,k=0;;
	int exist[arr.size()];
	for(int i=0;i<arr.size();i++){
		 if(containsVal(exist,arr.at(i),k)){
		    exist[k++]=arr.at(i);		
		    count=1;		
		    for(int j=i+1;j<arr.size();j++){
			      if(arr.at(i)==arr.at(j)){
			         count++;
			      }
		    }
		    if(count%2!=0){
			      return arr.at(i);
		    }
	 }
 }
}
int main(){
	int n,temp;
	vector<int> vec;
	std::cout<<"Enter n:"<<std::endl;
	std::cin>>n;
	std::cout<<"Enter the values:"<<std::endl;
	for(int i=0;i<n;i++){
		std::cin>>temp;
		vec.push_back(temp);
	}
	std::cout<<findOdd(vec)<<std::endl;
	
}

