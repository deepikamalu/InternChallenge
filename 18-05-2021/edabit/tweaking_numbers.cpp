#include<iostream>
#include<string>
#include <vector>
using namespace std;
string tweakLetters(string s,std::vector<int> arr) {
  char ch;
  string str;
  int k=0;
  for(int i=0;i<s.length();i++){
    ch=s.at(i);
    if(arr.at(k)==0){
      str.append(1,ch);
    }
    else if(arr.at(k)==1){
      if(ch=='z'){
        str.append(1,'a');
      } 
      else{ 
        str.append(1,++ch);
      } 
    }
    else{
      if(ch=='a'){
        str.append(1,'z');
      } 
      else{
        str.append(1,--ch);
      } 
    }
    k++;
  }
  return str;
}
int main(){
	string str;
	int n,temp;
	vector<int> vec;
	std::cout<<"Enter the string:"<<std::endl;
	std::cin>>str;
	std::cout<<"Enter n:"<<std::endl;
	std::cin>>n;
	std::cout<<"Enter the values:"<<std::endl;
	for(int i=0;i<n;i++){
		std::cin>>temp;
		vec.push_back(temp);
	}
	std::cout<<tweakLetters(str,vec)<<std::endl;
}
