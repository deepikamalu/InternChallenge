#include<iostream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;
std::vector<string> makeBox(int n) {
	std::vector<string> vec;
	string str;
	for(int i=0;i<n;i++){
		str="";
			for(int j=0;j<n;j++){
					if(i==0||i==n-1){
						 str.append(1,'#');
					}
				 else{
					 if(j==0||j==n-1){
					   str.append(1,'#');
					 }
					 else{
					   str.append(1,' ');
					 }
				 }	  	
	   }
		 vec.push_back(str);
	}
	return vec;
}
int main(){
	int n;
	std::cout<<"Enter n:"<<std::endl;
	std::cin>>n;
	vector<string> vec;
	vec=makeBox(n);
	for(int i=0;i<vec.size();i++){
	    std::cout<<vec.at(i);
	}
}
