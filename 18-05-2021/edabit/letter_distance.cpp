#include<iostream>
#include<string>
#include<cmath>
using namespace std;
int len(int a,int b){
	int min;
	if(a==b){
		min=a;
	}
	else if(a>b){
		min=b;
	}
	else{
		min=a;
	}
	return min;
}
int letterDistance(string str1,string str2) {
	int totalCount=0;
	int n=len(str1.length(),str2.length());
	for(int i=0;i<n;i++){
			totalCount+=abs(((int)str1.at(i)) - ((int)str2.at(i)));
	}
	if(str1.length()!=str2.length()){
		totalCount+=abs(str1.length()-str2.length());
	}
	return totalCount;
}
int main(){
	string str1,str2;
	std::cout<<"Enter the string1:"<<std::endl;
	std::cin>>str1;
	std::cout<<"Enter the string2:"<<std::endl;
	std::cin>>str2;
	std::cout<<letterDistance(str1,str2)<<std::endl;
}
