#include<iostream>
#include<string>
class Random{
    public: 
    Random(const Random&)=delete;
    static Random& Get(){
        return s_instance;
    }
    static float Float(){
        return Get().IFloat();
    }
    private:
    float IFloat(){
        return m_Randomgenerator;
    }
    Random(){}
    float m_Randomgenerator=0.5f;
    static Random s_instance;
};
Random Random::s_instance;
int main(){
    float number=Random::Float();
    std::cout<<number<<std::endl;
}
