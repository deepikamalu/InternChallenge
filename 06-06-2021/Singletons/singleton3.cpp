#include<iostream>
#include<string>
class Random{
    public: 
    Random(const Random&)=delete;
    static Random& Get(){
        static Random s_instance;
        return s_instance;
    }
    static float Float(){
        return Get().IFloat();
    }
    private:
    float IFloat(){
        return m_Randomgenerator;
    }
    Random(){}
    float m_Randomgenerator=0.5f;
   
};

int main(){
    float number=Random::Float();
    std::cout<<number<<std::endl;
}
