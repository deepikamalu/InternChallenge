#include<iostream>
#include<vector>
int main(){
    std::vector<int> val={1,2,3,4,5};
    for(int i=0;i<val.size();i++){
        std::cout<<val.at(i)<<std::endl;
    }
    for(int i:val){
         std::cout<<i<<std::endl;
    }
    for(std::vector<int>::iterator it=val.begin();it!=val.end();it++){
        std::cout<<*it<<std::endl;
    }
}
