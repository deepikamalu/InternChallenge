#include<iostream>
#include<unordered_map>

int main(){
    using scoremap=std::unordered_map<std::string,int>;
    scoremap sr;
    sr["Deepika"]=1;
    sr["c++"]=2;
    for(scoremap::iterator it=sr.begin();it!=sr.end();it++){
        auto& key=it->first;
        auto& value=it->second;
        std::cout<<key<<"="<<value<<std::endl;
    }
}
