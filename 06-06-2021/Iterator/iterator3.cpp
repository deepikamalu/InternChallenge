#include<iostream>
#include<unordered_map>

int main(){
    using scoremap=std::unordered_map<std::string,int>;
    scoremap sr;
    sr["Deepika"]=1;
    sr["c++"]=2;
    for(auto kv:sr){
        auto& key=kv.first;
        auto& value=kv.second;
        std::cout<<key<<"="<<value<<std::endl;
    }
}
