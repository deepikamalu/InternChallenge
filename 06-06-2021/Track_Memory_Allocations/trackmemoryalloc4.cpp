#include<iostream>
#include<memory>
struct AllocationMetrics{
    int total_allocated=0;
    int total_freed=0;
    int currentusage(){
        return total_allocated-total_freed;
    }
};
static AllocationMetrics s;
void* operator new(size_t size){
    s.total_allocated+=size;
    return malloc(size);
}
void operator delete(void* memory,size_t size){
    s.total_freed+=size;
    free(memory);
}
struct Object{
    int x,y,z;
};
static void print(){
    std::cout<<"Memory usage:"<<s.currentusage()<<"bytes\n";
}
int main(){
    print();
    std::string str="Deepika";
    print();
    {
    std::unique_ptr<Object> obj=std::make_unique<Object>();
    print();
    }
    print();
}

