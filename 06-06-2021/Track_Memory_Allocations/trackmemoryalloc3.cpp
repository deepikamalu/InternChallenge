#include<iostream>
#include<memory>
void* operator new(size_t size){
    std::cout<<"Allocating"<<size<<"bytes\n";
    return malloc(size);
}
void operator delete(void* memory,size_t size){
    std::cout<<"Freeing"<<size<<"bytes\n";
    free(memory);
}
struct Object{
    int x,y,z;
};
int main(){
    {
    std::unique_ptr<Object> obj=std::make_unique<Object>();
    }
    std::string str="Deepika";
}

