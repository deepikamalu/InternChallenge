#include<iostream>
#include<string>
static int s_alloccnt=0;
void* operator new(size_t size){
    s_alloccnt++;
    std::cout<<"Allocating"<<size<<"bytes\n";
    return malloc(size);
}
void print(std::string_view name){
    std::cout<<name<<std::endl;
}
int main(){
    const char* name="Deepika";
    std::string_view fname(name,3);
    std::string_view lname(name+4,9);
    print("Malu");
    std::cout<<s_alloccnt<<"allocations"<<std::endl;
}
    
