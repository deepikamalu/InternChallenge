#include<iostream>
class Entity{
	public:
	    static const int n=5; 
	    int arr[n];
	    Entity(){
	    	std::cout<<"Enter the values:"<<std::endl;
		    for(int i=0;i<n;i++){
			    std::cin>>arr[i];
		    }
	    }
	    void print(){
		    for(int i=0;i<n;i++){
			    std::cout<<arr[i]<<std::endl;
		    }
	    }
};
int main(){
	Entity e;
	e.print();
}
