#include<iostream>
#include<string>
using namespace std;
class Entity{
	public:
		virtual string getname(string n1){
			std::cout<<"entity "<<n1<<std::endl;
		}
};
class Player:public Entity{
	public:
		string getname(string n2) override{
			std::cout<<"player "<<n2<<std::endl;
		}
};
void print(Entity* et){
	et->getname("fighter");
}
int main(){
	Entity* e=new Entity();
	print(e);
	Player* p=new Player();
	print(p);
}
