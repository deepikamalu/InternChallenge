#include<iostream>
#include<string>
using namespace std;
class Entity{
	public:
		virtual string getname(string n1){
			std::cout<<n1<<std::endl;
		}
};
class Player:public Entity{
	public:
		string getname(string n2) override{
			std::cout<<n2<<std::endl;
		}
};
int main(){
	Entity e;
	e.getname("Deepika");
	Player p;
	p.getname("Malu");
}
