#include<iostream>
class Entity{
	private:
	   int x=2;
	   mutable int var=5;
	public:
	   int getX() const{
	       var=2;
	       return x;
	   }   
	void setX(int n){
		x=n;
	}
};
void find(const Entity& et){
	std::cout<<et.getX()<<std::endl;
}
int main(){
	Entity e;
	e.setX(9);
	find(e);
}
