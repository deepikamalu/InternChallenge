#include<iostream>
#include<string>
using namespace std;
class Printable{
	public:
	    virtual string getClassName()=0;
};
class Entity:public Printable{
	public:
		virtual string getName(){
			return "Entity";
		}
		string getClassName() override{
		    return "Entity1";
		}
};
class Player : public Entity{
	public:
	    string getName()override{
	        return "Player"; 
	    }
	    string getClassName()override{
		    return "Player1";
		}
};
void printName(Entity* et){
	std::cout<<et->getName()<<std::endl;
}
class A:public Printable{
	public:
		string getClassName()override{
		    return "A1";
		}
};
void print(Printable* obj){
	std::cout<<obj->getClassName()<<std::endl;
}
int main(){
	Entity* e=new Entity();
	printName(e);
	Player* p=new Player();
	printName(p);
	print(e);
	print(p);
	print(new A());
}
