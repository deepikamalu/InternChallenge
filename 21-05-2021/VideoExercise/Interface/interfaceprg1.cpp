#include<iostream>
#include<string>
using namespace std;
class Entity{
	public:
	    virtual string getname()=0;
};
class Player:public Entity{
	public:
	    string getname()override{
		    std::cout<<"Deepika G P"<<std::endl;
	    }
};
void print(Entity* et){
	et->getname();
}
int main(){
	Entity* e=new Player();
	print(e);
	Player* p=new Player();
	print(p);
}

