#include<iostream>
int max(int a,int b){
	return (a>b)?a:b;
}
int battleOutcome(int num) {
	int temp=num,c=0,c1,c2,sum=0;
	while(temp!=0){
		c++;
		temp=temp/10;
	}
	char arr[c + sizeof(char)];
    sprintf(arr, "%d", num);
    for(int i=0;i<c-1;i=i+2){
    	    c1=arr[i]-'0';
			c2=arr[i+1]-'0';
			if(c1!=c2)	
			sum=sum*10+max(c1,c2);
	}
	if(c%2!=0){
		sum=sum*10+(arr[c-1]-'0');
	}
    return sum;
}
int main(){
	int num;
	std::cout<<"Enter the num:"<<std::endl;
	std::cin>>num;
	std::cout<<battleOutcome(num)<<std::endl;
}
