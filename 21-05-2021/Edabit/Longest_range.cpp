#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;
int maxConsec(std::vector<int> arr) {
	std::sort(arr.begin(),arr.end());
	int arr2[arr.size()],k1=1;
	arr2[0]=arr[0];
	for(int i=1;i<arr.size();i++){
		if(arr[i]!=arr[i-1]){
			arr2[k1++]=arr[i];
		}
	}
	int c=0,j;
	int arr1[arr.size()],k=0;
	for(int i=0;i<k1;i++){
		if(arr2[i+1]==arr2[i]+1){
			c++;
		}
		else{
			arr1[k]=c+1;
			
			k++;
			c=0;			
		}
	}
	arr1[k++]=c;
	return *max_element(arr1, arr1+k);
}
int main(){
	std::vector<int> vec;
	int n,t;
	std::cout<<"Enter n:"<<std::endl;
	std::cin>>n;
	std::cout<<"Enter the elements:"<<std::endl;
	for(int i=0;i<n;i++){
		std::cin>>t;
		vec.push_back(t);
	}
	std::cout<<maxConsec(vec)<<std::endl;
}
