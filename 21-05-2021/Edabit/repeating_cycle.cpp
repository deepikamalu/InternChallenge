#include<iostream>
#include<vector>
bool isRepeatingCycle(std::vector<int> arr, int length) {
	int arr1[length],c;
	for(int i=0;i<length;i++){
		arr1[i]=arr.at(i);
	}
	for(int i=length;i<arr.size();){
		c=0;
		for(int j=0;j<length;j++){
			if(i==arr.size()){
				break;
			}
			else if(arr1[j]==arr.at(i)){
				i++;
			}
			else{
				return false;
			}
		}
	}
	return true;
}
int main(){
	std::vector<int> vec;
	int len,n,t;
	std::cout<<"Enter n:"<<std::endl;
	std::cin>>n;
	std::cout<<"Enter the elements:"<<std::endl;
	for(int i=0;i<n;i++){
		std::cin>>t;
		vec.push_back(t);
	}
	std::cout<<"Enter the length:"<<std::endl;
	std::cin>>len;
	if(isRepeatingCycle(vec,len)){
	    std::cout<<"true"<<std::endl;
	}
	else{
		std::cout<<"false"<<std::endl;
	}
}
