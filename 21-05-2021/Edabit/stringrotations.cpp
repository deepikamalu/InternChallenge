#include<iostream>
#include<cstring>
#include <sstream> 
#include<vector>
using namespace std;
string find(char arr[]){
	string str;
	std::stringstream strStream;  
    strStream << arr;  
    strStream >> str;  
    return str; 
}
std::vector<string> leftRotations(string str) {
	string str1;
	char arr[str.length()+1],ch;
	strcpy(arr, str.c_str());
	std::vector<string> vec;
	vec.push_back(find(arr));
	for(int j=0;j<str.length()-1;j++){
		ch=arr[0];
	    for(int i=0;i<str.length()-1;i++){
		    arr[i]=arr[i+1];
	    }
	    arr[str.length()-1]=ch;
	    vec.push_back(find(arr));
    }
    return vec;
}

std::vector<string> rightRotations(string str) {
	string str1;
	char arr[str.length()+1],ch;
	strcpy(arr, str.c_str());
	std::vector<string> vec;
	vec.push_back(find(arr));
	for(int j=str.length()-1;j>=1;j--){
		ch=arr[str.length()-1];
	    for(int i=str.length()-1;i>=0;i--){
		    arr[i]=arr[i-1];
	    }
	    arr[0]=ch;
	    vec.push_back(find(arr));
    }
    return vec;
}
int main(){
	string str1;
	vector<string> v1,v2;
	std::cout<<"Enter the string:"<<std::endl;
	std::cin>>str1;
	v1=leftRotations(str1);
	v2=rightRotations(str1);
	std::cout<<"Enter the left rotation string:"<<std::endl;
	for(int i=0;i<v1.size();i++){
		std::cout<<v1.at(i)<<" ";
	}
	std::cout<<"\n"<<std::endl;
	std::cout<<"Enter the right rotation string:"<<std::endl;
	for(int i=0;i<v2.size();i++){
		std::cout<<v2.at(i)<<" ";
	}	
}
