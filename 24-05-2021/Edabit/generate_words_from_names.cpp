#include<iostream>
#include<string>
#include<algorithm>
#include<vector>
#include <cctype>
using namespace std;
bool anagram(std::string name, std::vector<std::string> words) {
	string str1,str2;
	for(int i=0;i<name.length();i++){
		if(name.at(i)!=' '){
			str1.append(1,(char)tolower(name.at(i)));
		}
	}
	sort(str1.begin(),str1.end());
	//std::cout<<str1<<std::endl;
	for(int i=0;i<words.size();i++){
		str2.append(words.at(i));
	}
	sort(str2.begin(),str2.end());
	//std::cout<<str2<<std::endl;
	if(str1.compare(str2)==0){
		return true;
	}
	else{
		return false;
	}	
}
int main(){
	string str;
	std::cout<<"Enter the string:"<<std::endl;
	getline(cin,str);
	int n;
	string t;
	std::vector<string> vec;
	std::cout<<"Enter n:"<<std::endl;
	std::cin>>n;
	for(int i=0;i<n;i++){
		std::cin>>t;
		vec.push_back(t);
	}
	if(anagram(str,vec)){
		std::cout<<"true"<<std::endl;
	}
	else{
		std::cout<<"false"<<std::endl;
	}
}
