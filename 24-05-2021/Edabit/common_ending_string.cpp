#include<iostream>
#include<string>
#include<algorithm>
#include<vector>
#include <cctype>
using namespace std;
string longestCommonEnding(string s1, string s2) {
	reverse(s1.begin(),s1.end());
	reverse(s2.begin(),s2.end());
	string str;
	for(int i=0;s1.at(i)!='\0' && s2.at(i)!='\0';i++){
		if(s1.at(i)==s2.at(i)){
			str.append(1,s1.at(i));
		}
		else{
			break;
		}
	}
	reverse(str.begin(),str.end());
	return str;
}
int main(){
	string str1,str2;
	std::cout<<"Enter the string1:"<<std::endl;
	getline(cin,str1);
	std::cout<<"Enter the string2:"<<std::endl;
	getline(cin,str2);
	std::cout<<longestCommonEnding(str1,str2)<<std::endl;
}
