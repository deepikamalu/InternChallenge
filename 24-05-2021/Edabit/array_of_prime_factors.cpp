#include<iostream>
#include<vector>
using namespace std;
std::vector<int> prime(int num){
	int c;
	std::vector<int> v1;
	for(int i=2;i<=num;i++){
		c=0;
		for(int j=1;j<=i/2;j++){
			if(i%j==0){
			c++;
			}
		}
		if(c==1){
			v1.push_back(i);
		}
	}
	return v1;
}
std::vector<int> primeFactorization(int number) {
	std::vector<int> vec=prime(number);
	std::vector<int> vec1;
	for(int i=0;i<vec.size() && number!=0;i++){
		while(number%vec.at(i)==0 && number!=0){
			vec1.push_back(vec.at(i));
			number=number/vec.at(i);
		}
	}
	return vec1;
}
int main(){
	int n;
	std::cout<<"Enter the number:"<<std::endl;
	std::cin>>n;
	std::vector<int> vec1=primeFactorization(n);
    for(int i=0;i<vec1.size();i++){
    	std::cout<<vec1.at(i)<<" "<<std::endl;
    }  
}

