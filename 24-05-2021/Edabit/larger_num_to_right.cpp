#include<iostream>
#include<vector>
using namespace std;
std::vector<int> largerThanRight(std::vector<int> arr) {
	std::vector<int> vec;
	int c;
	for(int i=0;i<arr.size()-1;i++){
		c=0;
		for(int j=i+1;j<arr.size();j++){
			if(arr.at(i)>arr.at(j)){
				c++;
			}
		}
		if(c==arr.size()-(i+1)){
			vec.push_back(arr.at(i));
		}
	}
	vec.push_back(arr.at(arr.size()-1));
	return vec;
}
int main(){
	int n,t;
	std::vector<int> vec;
	std::cout<<"Enter n:"<<std::endl;
	std::cin>>n;
	std::cout<<"Enter the values:"<<std::endl;
	for(int i=0;i<n;i++){
		std::cin>>t;
		vec.push_back(t);
	}
	std::vector<int> vec1=largerThanRight(vec);
    for(int i=0;i<vec1.size();i++){
    	std::cout<<vec1.at(i)<<" "<<std::endl;
}
}
