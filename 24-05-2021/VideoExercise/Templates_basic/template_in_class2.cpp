#include<iostream>
#include<string>
template<typename T,int N>
class Entity{
	private:
		T arr[N];
	public:
	    Entity(){
	    	for(int i=0;i<N;i++){
	    		std::cin>>arr[i];
			}
		}
		void print() const{
			for(int i=0;i<N;i++){
	    		std::cout<<arr[i]<<" ";
			}
		}
};
int main(){
	Entity<int,5> arr1;
	arr1.print();
}
