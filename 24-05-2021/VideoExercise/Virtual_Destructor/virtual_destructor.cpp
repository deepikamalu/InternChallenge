#include<iostream>
#include<string>
#include<vector>
using namespace std;
class Base{
	public:
		Base(){
			std::cout<<"Base Constructor"<<std::endl;
		}
		virtual ~Base(){
			std::cout<<"Base Destructor"<<std::endl;
		}
};
class Derived{
	public:
		Derived(){
			std::cout<<"Derived Constructor"<<std::endl;
		}
		~Derived(){
			std::cout<<"Derived Destructor"<<std::endl;
		}
};
int main(){
	Base* b=new Entity();
	delete b;
}
