#include<iostream>
#include<string>
#include<vector>
using namespace std;
class Base{
	public:
		Base(){
			std::cout<<"Base Constructor"<<std::endl;
		}
		virtual ~Base(){
			std::cout<<"Base Destructor"<<std::endl;
		}
};
class Derived:public Base{
	int* arr=new int[5];
	public:
		Derived(){
			for(int i=0;i<5;i++){
	    		std::cin>>arr[i];
			}
		}
		~Derived(){
			for(int i=0;i<5;i++){
	    		std::cout<<arr[i]<<" ";
			}
			std::cout<<std::endl;
			delete arr;
		}
};
int main(){
	Base* b=new Derived();
	delete b;
}
